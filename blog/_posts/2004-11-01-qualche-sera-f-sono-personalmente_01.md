---
layout: post
title: Il ricordo di belle cose
date: '2004-11-01T23:18:00.001+01:00'
author: Luca Orlandi
lang: it-IT
tags:
modified_time: '2006-10-26T01:02:59.879+02:00'
blogger_id: tag:blogger.com,1999:blog-5600070.post-114295162361443136
blogger_orig_url: http://lrkwz.blogspot.com/2004/11/qualche-sera-f-sono-personalmente_01.html
---

Qualche sera fa ho visto "[Il ricordo di belle cose](http://www.sesouvenirdesbelleschoses.com/)" Z. Breitman FRA 2002, un'opera prima assolutamente degna e intrigante; è la storia lieve di una scelta d'amore per una trentenne che si scopre essere affatta dal morbo di [Alzhaimer](http://www.alzheimer.it/), la storia termina lasciano comprendere molto chiaramente quali possano essere le senzazioni di una persona che soffre di questa malattia: lo sconforto, la paura e l'impossibilità di comprendere gli altri e il 'fuori da sè' in genere sono palpabili.

Sicuramente il film mi ha profondamente colpito anche perchè sono personalmente vicino a questo problema.