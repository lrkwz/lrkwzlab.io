---
layout: post
title: Hudson is your (new) friend
date: '2010-04-18T01:05:00.001+02:00'
author: Luca Orlandi
tags:
- hudson
- java
- continuous integration
modified_time: '2010-04-18T01:07:09.667+02:00'
blogger_id: tag:blogger.com,1999:blog-5600070.post-1286866659359567934
blogger_orig_url: http://lrkwz.blogspot.com/2010/04/hudson-is-your-new-friend.html
---

Da qualche tempo ho iniziato ad utilizzare intensivamente [Hudson](http://hudson-ci.org/); tranquilli non mi metterò qui certo a raccontare il come e il perchè della continuous integration.

Da subito sono rimasto sbalordito per la qualità di questo software: installazione semplicissima, documentazione semplice e precisa, interfaccia intuibile.
Dopo il primo avvio il resto si installa e configura 100% dalla interfaccia browser, non è praticamente mai necessario connettersi alla macchina.

Dapprima ovviamente mi sono accontentato di configurare un singolo nodo sulla piattarofma di riferimento più comune per i miei progetti, ma dopo poco ho avuto la necessita di utilizzare anche sistemi diversi ...Windows poor me! quindi mi sono avventurato nella configurazione multinodo ... sbalorditivo! dopo un primo intoppo legato al networking (il nodo master è dislocato su una macchina pubblica mentre gli slave su macchine interne) la configurazione degli slave è andata in un batter d'occhio.

La prima e finora unica pecca è stata l'installazione automatica dell'sdk java (usando s.o. diversi è obbligatorio usare l'installazione automatica non fosse altro per i path): il jdk si installa per default in `C:\Program Files\Java` mentre hudson lo cerca nella sua working dir sotto `tools\java` ... poco male: ho copiato tutti i files dalla installazione di default e tutto ha ripreso a funzionare.