---
layout: post
title: Why MY code sucks
date: '2004-10-01T13:47:00.000+02:00'
author: Luca Orlandi
lang: it-IT
tags:
modified_time: '2006-10-26T01:02:58.567+02:00'
blogger_id: tag:blogger.com,1999:blog-5600070.post-109662765365690564
blogger_orig_url: http://lrkwz.blogspot.com/2004/10/larticolo-why-your-code-sucks-mi-trova.html
---

L'articolo [Why your code sucks](http://www.theserverside.com/news/thread.tss?thread_id=29114) mi trova completamente d'accordo:

*   Your code sucks if it doesn't work.
*   Your code sucks if it isn't testable.
*   Your code sucks if it's hard to read.
*   Your code sucks if it's not understandable.
*   Your code sucks if it dogmatically conforms to a trendy framework at the cost of following good design/implimentation practices.
*   Your code sucks if it has duplication.

E cosa posso dire: "MY code sucks as well" :-|