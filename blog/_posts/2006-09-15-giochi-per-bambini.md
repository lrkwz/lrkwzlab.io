---
layout: post
title: Giochi per bambini
date: '2006-09-15T23:41:00.000+02:00'
author: Luca Orlandi
tags:
modified_time: '2006-10-26T01:03:00.969+02:00'
blogger_id: tag:blogger.com,1999:blog-5600070.post-115835650500278107
blogger_orig_url: http://lrkwz.blogspot.com/2006/09/giochi-per-bambini.html
---

Avrei voluto organizzare una caccia al tesoro per la festa di Matilde domani, ci ho pensato tutto il giorno ma ... solo adesso realizzo che compie solo 5 anni e quindi nè lei nè la sue amiche sanno ancora leggere!

Mi accontento per ora con [giocomania](http://www.giocomania.org).