---
layout: post
title: jasypt re-cryptor
date: '2011-06-23T12:58:00.000+02:00'
author: Luca Orlandi
lang: it-IT
tags:
- hibernate
- spring
- crypting
- java
modified_time: '2011-09-14T22:14:43.317+02:00'
blogger_id: tag:blogger.com,1999:blog-5600070.post-2476434983659199351
blogger_orig_url: http://lrkwz.blogspot.com/2011/06/jasypt-re-cryptor.html
---

Mi trovo a dovere applicare la crittazione ad un  database già esistente, purtroppo non ho potuto optare per una soluzione sistemistica (tipo le oracle crypting option) quindi ho ripiegato su una soluzione 100% applicativa.

L'ottima libreria jasypt si "inietta" tra lo strato applicativo vero e proprio e  hibernate in modo da rendere il processo di (de)crittazione trasparente dal punto di vista applicativo:
dopo avere configurato i bean a supporto del crypting è sufficiente aggiungere un paio di annotazioni alle proprietà delle classi di dominio che devoo essere "protette" ed il gioco è fatto!
i problemi sorgono quando questo intervento deve essere fatto su un database di produzione già popolato ... per risolvere la questione uno degli approcci possibili consiste nel definire un semplice wrapper della classe PBEStringEncryptor che non fa altro che delegare e a fronte della eccezione EncryptionOperationNotPossibleException restituire il dato originale:

```java
import org.jasypt.encryption.pbe.PBEStringEncryptor;
import org.jasypt.exceptions.EncryptionOperationNotPossibleException;

public class RecryptingPooledPBEStringREncryptor implements PBEStringEncryptor {
    private PBEStringEncryptor firstEncryptor;

    @Override
    public String encrypt(String message) {
        return firstEncryptor.encrypt(message);
    }

    @Override
    public String decrypt(String encryptedMessage) {
        try {
            return firstEncryptor.decrypt(encryptedMessage);
        } catch (EncryptionOperationNotPossibleException e) {
            return encryptedMessage;
        }
    }

    @Override
    public void setPassword(String password) {
        firstEncryptor.setPassword(password);
    }

    /**
     * @return the firstEncryptor
     */
    public PBEStringEncryptor getFirstEncryptor() {
        return firstEncryptor;
    }

    /**
     * @param firstEncryptor
     *            the firstEncryptor to set
     */
    public void setFirstEncryptor(PBEStringEncryptor firstEncryptor) {
        this.firstEncryptor = firstEncryptor;
    }
}
```

Questo bean può essere dichiarato nell'applicationContext.xml attorno allo stringencryptor vero e proprio:

```xml
<bean id="strongEncryptor"
  class="it.more.encryption.pbe.RecryptingPooledPBEStringREncryptor">
  <property name="firstEncryptor">
   <bean id="pbeStringEncryptor" class="org.jasypt.encryption.pbe.PooledPBEStringEncryptor">
    <property name="algorithm">
     <value>PBEWITHSHA256AND128BITAES-CBC-BC</value>
    </property>
    <property name="password">
     <value>jasypt</value>
    </property>
    <property name="poolSize">
     <value>4</value>
    </property>
    <property name="provider" ref="bouncyProvider"></property>
    <property name="saltGenerator">
     <bean class="org.jasypt.salt.ZeroSaltGenerator" />
    </property>
   </bean>
  </property>
 </bean>
```

