---
layout: post
title: Panificio Automatico Continuo
date: '2006-09-19T22:47:00.000+02:00'
author: Luca Orlandi
tags:
modified_time: '2006-10-26T01:03:01.093+02:00'
blogger_id: tag:blogger.com,1999:blog-5600070.post-115869887022959226
blogger_orig_url: http://lrkwz.blogspot.com/2006/09/panificio-automatico-continuo.html
---

Oggi andando in giro in moto con il naso per aria mi sono accorto del Panificio Automatico Continuo; si tratta di un edificio dall'aspetto pulito ed imponente con questa dicitura impressa sulla facciata.

Ho trovato qualche riferimento e un paio di foto ma nulla di più: c'è nessuno che ne conosce la storia?