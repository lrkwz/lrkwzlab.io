---
layout: post
title: Stele di Rosetta
date: '2004-10-19T10:24:00.000+02:00'
author: Luca Orlandi
lang: it-IT
tags:
modified_time: '2006-10-26T01:02:58.747+02:00'
blogger_id: tag:blogger.com,1999:blog-5600070.post-109817425076923430
blogger_orig_url: http://lrkwz.blogspot.com/2004/10/saltabeccando-da-un-sistema-operativo.html
---

Saltabeccando da un sistema operativo all'altro può tornare piuttosto utile avere sottomano la [stele di rosetta per Unix](http://bhami.com/rosetta.html)