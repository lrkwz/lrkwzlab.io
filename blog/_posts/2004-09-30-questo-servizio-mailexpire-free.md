---
layout: post
title: Mailexpire
date: '2004-09-30T18:29:00.000+02:00'
author: Luca Orlandi
lang: it-IT
tags:
modified_time: '2006-10-26T01:02:58.436+02:00'
blogger_id: tag:blogger.com,1999:blog-5600070.post-109656175196890937
blogger_orig_url: http://lrkwz.blogspot.com/2004/09/questo-servizio-mailexpire-free.html
---

Questo servizio [mailexpire - free temporary email addresses...](http://www.mailexpire.com/) consente di generare delle caselle postali a tempo per evitare di essere spammati.