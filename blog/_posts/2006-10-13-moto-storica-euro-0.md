---
layout: post
title: Moto storica ... EURO 0!
date: '2006-10-13T19:05:00.000+02:00'
author: Luca Orlandi
tags:
- Personal
modified_time: '2006-10-26T01:03:01.551+02:00'
blogger_id: tag:blogger.com,1999:blog-5600070.post-116075914400766774
blogger_orig_url: http://lrkwz.blogspot.com/2006/10/moto-storica-euro-0.html
---

Sinceramente non sono riuscito ancora a capire se la regione Lombardia bloccherà o meno le moto 4 tempi euro 0.

Un [comunicazione](http://snipurl.com/vvkd) sul sito della regione parla di moto 2 tempi e non si citano le 4 tempi.

Il mio meccanico mi ha detto che probabilmente già da gennaio non sarà possibile circolare ...

E' mai possibile che la regione decida di fermare il parco moto circolante e serenamente accetti mezzi pesanti, diesel di ogni genere in città e ancora caldaie a gasolio a tutto spiano nei centri storici?

Sono rimasto colpito dalla [innovazione tecnologica](http://www.treehugger.com/files/2006/10/america_says_he_1.php) relativamente ai motori diesel: dei laboratori chimici a 4 ruote che generano ammoniaca (ammonia) per abbatterne l'inquinamento.