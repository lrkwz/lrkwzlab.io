---
layout: post
title: Gioie e dolori delle organizzazioni piatte.
date: '2005-10-06T11:13:00.000+02:00'
author: Luca Orlandi
lang: it-IT
tags:
modified_time: '2006-10-26T01:02:59.214+02:00'
blogger_id: tag:blogger.com,1999:blog-5600070.post-112859001090622933
blogger_orig_url: http://lrkwz.blogspot.com/2005/10/gioie-e-dolori-delle-organizzazioni.html
---

L'azienda dove lavoro (oramai da tantissimo tempo) ha a più riprese fatto sforzi per assumere via via una forma matriciale, poi piatta ... devo anche dire che per fortuna non è mai stato fatto uno sforzo per la creazione di una organizzazione fortemente gerarchizzata: è un modello con il quale faccio una certa fatica a relazionarmi. sono incappato in un [post recente](http://www.jrothman.com/weblog/2005/09/small-rant-about-flat-organizations.html) relativo al management su alle cosiddette strutture piatte.

Sinceramente non sono sicuro che la direzione intrapresa in questo momento sia nella direzione indicata dall'autore del post ma mi sembra che alcune scelte siano allineate ... chissà se è una fortuna?

PS: probabilmente questi pensieri mi vengono a causa della frequentazione di un bel corso rivolto ai manager intermedi (forse la dicitura "quadri" indica bene il concetto ... anche se rievoca frasi del tipo "...i quadri del partito").