---
layout: post
title: Nomi delle variabili e dei campi in Oracle pl/sql
date: '2008-02-11T22:08:00.000+01:00'
author: Luca Orlandi
tags:
modified_time: '2008-02-11T22:49:13.540+01:00'
blogger_id: tag:blogger.com,1999:blog-5600070.post-8025199280971469127
blogger_orig_url: http://lrkwz.blogspot.com/2008/02/nomi-delle-variabili-e-dei-campi-in.html
---

Ancora una volta mi sono fatto fregare come un bambino: la risoluzione dei nomi delle variabili delle stored procedure e delle funzioni pl/sql di Oracle ha un comportamento che dimentico sempre di prevedere:

"i nomi delle variabili e i nomi dei campi delle tabelle **devono** essere differenti"

In altre parole, l'affermazione "i nomi dei campi devono essere unici nello schema" deve essere presa alla lettera ... sempre.

Per esempio:

```sql
function conta( a varchar2 ) return number is

  v number;

begin

   select count(*) into v

   from tlb

   where tbl.a = a;

   return v;

end;

```

impone alla select una condizione sempre vera: cioè è come scrivere a=a ... quindi la variabile a deve avere un altro nome ...

Questa volta non lo scordo più.

Grazie Silvia!

PS: Un'altra cosa che trovo terribilmente irritante è il dovere dichiarare i nomi delle variabili nell'header del package esattamente identici a quelli usati internamente al package.

PSS: Temo che dovrò adottare una rigorosissima (e noiosissima) convenzione `v_` le variabili, `i_` i parametri in input e `o_` per i parametri in output :-<