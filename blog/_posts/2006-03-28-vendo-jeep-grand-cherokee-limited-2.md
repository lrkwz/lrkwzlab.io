---
layout: post
title: Vendo Jeep
date: '2006-03-28T09:34:00.000+02:00'
author: Luca Orlandi
lang: it-IT
tags:
modified_time: '2006-10-26T01:02:58.877+02:00'
blogger_id: tag:blogger.com,1999:blog-5600070.post-112004507287511597
blogger_orig_url: http://lrkwz.blogspot.com/2006/03/vendo-jeep-grand-cherokee-limited-2.html
---

Vendo Jeep Grand Cherokee Limited 2.5 TD Quadra Trac
8.999 € trattabili
Immatricolazione 09/1998
Percorrenza: 98.000 km
Carrozzeria in ordine a parte un graffio sulla conchiglia specchietto destro.
Tenuta sempre in box.
Revisione Dicembre 2004
Vendo l’auto a causa di sostanziale inutilizzo.

[![](http://photos1.blogger.com/blogger/8036/83/320/DSCF0008.jpg)](http://photos1.blogger.com/blogger/8036/83/1600/DSCF0008.jpg)
4 Ruote Motrici,
Cerchi in Lega,
Chiusura Centralizzata,
Climatizzatore automatico,
Cruise Control al volante,
Gancio traino rimovibile,
Immobilizzatore elettronico,
Impianto Hi-Fi,
Inserti in radica,
Interni in Pelle,
Marmitta Catalitica,
Memorizzazione posizione sedili e specchietto con la chiave,
Radio Cassetta,
Sedile posteriore sdoppiato,
Sedili elettrici ,
Servosterzo,
Specchi elettrici,
Tendalino copertura bagagliaio,
Tergilunotto,
Vernice metallizata,
Vetri elettrici anteriori e posteriori
Volante in pelle multifunzione

Contatta [Luca](mailto:luca.orlandi@gmail.com) oppure telefona 3356294298