---
layout: post
title: Shortcut per gvim
date: '2006-11-14T23:37:00.001+01:00'
author: Luca Orlandi
tags:
- Tech
modified_time: '2006-11-14T23:37:25.079+01:00'
blogger_id: tag:blogger.com,1999:blog-5600070.post-5743751695241288474
blogger_orig_url: http://lrkwz.blogspot.com/2006/11/shortcut-per-gvim.html
---

Da quando ho sostituito notepad con gvim questi sono i miei shortcut preferiti:

> " Copy to 'clipboard registry'
> vmap <C-C> "\*y
>
> " Search selected text
> vmap / y/<C-R>"<CR>
>
> " Select all text
> nmap <C-A> 1G^vG$

aggiungili al tuo file $HOME/.gvimrc.