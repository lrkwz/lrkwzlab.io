---
layout: post
title: Googlepages
date: '2006-03-22T14:56:00.000+01:00'
author: Luca Orlandi
lang: it-IT
tags:
- Tech
modified_time: '2006-10-26T01:03:00.096+02:00'
blogger_id: tag:blogger.com,1999:blog-5600070.post-114303578418504302
blogger_orig_url: http://lrkwz.blogspot.com/2006/03/googlepages.html
---

Hanno finalmente abilitato il mio account googlepages, per esperimento ho fatto una homepage visibile a questo indirizzo [http://luca.orlandi.googlepages.com/](http://luca.orlandi.googlepages.com/).

Il sistema è fornisce un meccanismo di definizione di stile e layout con un editor [WYSIWYG](http://en.wikipedia.org/wiki/WySiWyG); è una sorta di "microsoft frontpage" (esiste ancora?) web based.

Per ora non ci mettono la pubblicità, quindi perchè lo fanno? Staremo a vedere.