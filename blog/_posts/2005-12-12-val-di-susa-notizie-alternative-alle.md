---
layout: post
title: Val di Susa - notizie alternative (alle poche ufficiali)
date: '2005-12-12T12:55:00.000+01:00'
author: Luca Orlandi
lang: it-IT
tags:
modified_time: '2006-10-26T01:02:59.403+02:00'
blogger_id: tag:blogger.com,1999:blog-5600070.post-113438851537776900
blogger_orig_url: http://lrkwz.blogspot.com/2005/12/val-di-susa-notizie-alternative-alle.html
---

Ricevo e rigiro:

Val di Susa - notizie alternative (alle poche ufficiali)

Qui viviamo con gli scarponi ai piedi anche in casa.
Viviamo sbirciando dalla finestra quanti mezzi della polizia sono intorno a noi e quanti stanno andando a raggiungere "il fronte".
Viviamo costretti a dare i documenti per andare a casa nostra.
Viviamo con telefoni accesi e sorvegliati. Viviamo schedati, scortati, lindati.
Ma soprattutto, viviamo con gli scarponi ai piedi anche in casa per dare il cambio a chi da giorni resiste nella neve e per accorrere alla chiamata di chi sta difendendo le barricate, ogni volta che provano a sfondare per tentare di far partire i cantieri più inutili della storia d'Italia.
Non li fermeremo? Questo non lo sappiamo, ma almeno potremo testimoniare.

Sono anni che ci nascondono la verità, che ci infangano e ora, come se non bastasse, ci hanno anche umiliato pubblicamente definendoci terroristi e nemici dell'Italia.

E' per questo che, traditi dalle stesse persone che ci rappresentano, insultati dai mezzi di informazione, ci rivolgiamo a voi personalmente, facendo appello alle vostre coscienze, perché questo è l'ultimo mezzo che abbiamo per far sentire la nostra voce vera e per difendere quel poco di democrazia che è rimasta.

Siamo il cosiddetto popolo no tav.
Quei rompiscatole, bollati come nemici dello sviluppo, che si battono perché, a detta dei nostri ministri, non hanno nulla di meglio da fare.

No signori, le cose non stanno così, vi prego credeteci, le cose non stanno così.

Dietro il no tav non c'è la protesta contro il progresso, ma c'è la precisa protesta contro uno specifico progetto suicida.
Un progetto deciso a tavolino che ha deliberatamente ignorato tutte le alternative possibili che in questi anni abbiamo fornito e che consentirebbero la realizzazione della linea ferroviaria ad alta velocità senza questo folle disastro ambientale.

Il motivo di questa mail è semplice quanto triste.

Stiamo cercando di far emergere la verità che sta dietro a quest'opera, e crediamo purtroppo anche ad altre opere italiane, nonostante i ripetuti sforzi di alcune testate giornalistiche e televisive di distogliere l'attenzione dalla realtà delle cose per difendere gli interessi enormi di poche, importantissime persone.

Quello su cui vi invitiamo a riflettere è semplicemente questo:

Perché nessuno sulle testate nazionali, che si definiscono super partes, ha ancora detto come stanno realmente le cose?
Perché continuano a dipingerla come una lotta di contadini contro lo sviluppo?
O peggio ancora come una volontà di isolare l'Italia.
Perché nessuno ha ancora parlato con chiarezza dei reali interessi economici in gioco?
Perché gli studi ufficiali sulla pericolosità spariscono all'improvviso nel nulla?
Ma soprattutto diteci vi prego, perché, anche in sede europea, non vengono vagliati progetti alternativi, che passerebbero sempre di qui ma senza fare il tunnel killer?

Purtroppo le risposte a queste domande ci sono eccome...

Sono nero su bianco, su documenti ufficiali, archiviate e prontamente occultate.
E la cosa più umiliante è che chi le conosce bene ed ha la possibilità di parlarne non lo fa.
Non lo fa per interesse e non lo fa perché comunque è letteralmente imbavagliato, da interessi che stanno troppo su per poter essere attaccati.

Noi invece abbiamo una forza che loro non hanno: non abbiamo più nulla da perdere.

Tanto cosa dovremmo fare?
I ministri ci insultano, chi tutela le istituzioni ci tradisce, chi si dovrebbe opporre tace, e chi è preposto a raccontare i fatti dipinge con devozione verità distorte.

Solo questa via c'è rimasta. Informare di persona.
Cercando di far conoscere ciò che sta nero su bianco e che viene nascosto.
Informare di persona.
Attingendo unicamente a documenti e dichiarazioni ufficiali e documentate.
E invitando a riflettere su quanto sia emblematica la concordia dei mass media nel distogliere l'attenzione dai reali interessi economici in gioco.

E' per questo quindi che vi rubo ancora qualche secondo di attenzione.
Per darvi i link ove potrete iniziare a saperne di più riguardo quest'opera.
Vi prego, consultateli, non lasciateci soli a combattere contro Golia.
Perché scoprirete anche che fine stanno facendo i nostri soldi senza che se ne sappia nulla.
Vedere chi c'è dietro a tutto ciò è sconcertante, ma forse si riuscirà a capire perché i giornali non vogliono o semplicemente non possono ancora raccontare tutto.

Come forse qualcuno saprà il nodo cruciale della questione per noi NON E' la realizzazione del corridoio cinque.
Noi non ci stiamo opponendo al corridoio cinque.
Lo abbiamo detto anche in diretta tv (ambiente italia).
E abbiamo detto chiaramente anche che non vogliamo che il treno non passi da noi per farlo passare sulla terra di altri. Sappiamo che il treno deve passare.
Non ci opponiamo a un treno. Sappiamo di essere nel 2005.
La smettano di dirci che siamo cretini che vogliono isolare l'Italia!

Quello a cui ci opponiamo sono i due tunnel killer che si vogliono realizzare.

1 - Perché sono un disastro ambientale
2 - Perché dietro c'è un progetto che fa vergognare di essere italiani...

Come saprete gli scontri stanno avvenendo per opporsi alla realizzazione del tunnel italo-francese.
Ci hanno insultati dicendo che non capiamo che il tunnel evita danni all'ambiente.
Ci hanno dato degli stupidi.

Ecco la verità.

Ecco qui di seguito link alle informazioni che sono misteriosamente sparite da tv e giornali (se non riuscite a cliccare direttamente sui link, copiateli e
incollateli nella barra indirizzi del browser)

A spartirsi il grosso della torta saranno due ditte, una per la Francia e l'altra per l'Italia.

Ecco chi c'è dietro, seguite tutti i link che vi elenco qui sotto

[http://www.notav.it/modules.php?name=News&file=article&sid=799](http://www.notav.it/modules.php?name=News&file=article&sid=799)

Questo è il versante francese... vi siete mai chiesti perché in tv fanno vedere solo francesi sorridenti che vogliono il tgv mentre in realtà sono già partite numerose proteste per i danni alle dovuti agli scavi... chi mai avrà interesse
nel far vedere che in Francia sono favorevoli...

[http://www.notav.it/modules.php?name=Encyclopedia&amp;amp;op=content&tid=2](http://www.notav.it/modules.php?name=Encyclopedia&amp;op=content&tid=2)

notate il curriculum da sant'uomo che si è preparata e poi fate un salto
alla sezione "dicono di lui" in fondo a quella pagina e capirete molte cose

E per il versante italiano questa è la ditta incaricata dei lavori qui a Venaus

[http://www.notav.it/modules.php?name=News&amp;amp;file=article&sid=789](http://www.notav.it/modules.php?name=News&amp;amp;file=article&sid=789)

vi suona nuova... non credo... guardate qui...

[http://www.cmc.coop/article.php?sid=119&amp;mode=thread&order=0&thold=0](http://www.cmc.coop/article.php?sid=119&amp;amp;mode=thread&order=0&thold=0)

Viva l'Italia! E' questo il vero scandalo legato al tunnel!
Asse trasversale come ai vecchi tempi pur di mangiare tutti dallo stesso piatto!
Bentornati nell'Italia dei faccendieri!

E ovviamente interessi da parte di chi poi ci mette macchinari, tecnologia ecc...
Insomma una fetta bella grossa di soldi, lo ripeto solo in parte europei, da spartirsi secondo un piano preordinato già da tempo.

Adesso si capisce perché anche le testate giornalistiche, inutile parlare di quelle televisive, solitamente schierate su posizioni opposte si sono ritrovate d'accordo nel condannare il popolo no tav.
Ecco perché soltanto alcuni quotidiani, non legati a schieramenti o imprese che hanno interessi economici nella vicenda, hanno conservato una certa obiettività nel raccontare i fatti.

Politici che si accalcano a bollarci come nemici del progresso, come terroristi!
Da destra e da sinistra, ma non sorgevano alcuni sospetti... cavolo!
Ed ecco la verità, schifosa e sconcertante come sempre!

Ora capite perché i progetti alternativi al tunnel che chiediamo con forza sono sempre stati bollati come non realizzabili.
Non c'era nulla da spartirsi!
L'adeguamento della linea attuale costerebbe molto meno del tunnel e lo si realizzerebbe in molti meno anni, quindi, perché farlo? Cosa ci dividiamo se la torta è piccolina!

E noi a ripetere. Lo sappiamo che il treno dovrà passare.
Decidiamolo insieme il tracciato.
Le conosciamo bene le caratteristiche geologiche delle montagne.
E invece no!
Il tracciato è questo si fa il tunnel punto e basta.
L'uranio, l'amianto, l'instabilità della montagna (le stesse gallerie dell'autostrada stanno franando lentamente)... affari vostri.
A noi per ora interessa aprire i cantieri.
Anche solo per far girare una trivella a vuoto.
Almeno cominciamo a prendere i soldi.
Poi si vedrà...

Ora capite perché ci sentiamo umiliati e traditi!

Ma invece di ascoltarci:

1.  Si sono spartiti equamente la torta
2.  Hanno invaso la valle di militari per portare i macchinari
3.  Ci impediscono anche la mobilità all'interno delle stesse aree dove abitiamo.
4.  Hanno iniziato la più vergognosa delle campagne diffamatorie.
    Italiani contro Italiani!
5.  Chiediamo da anni di approvare i progetti alternativi che tengano conto dei pericoli ambientali

Ci hanno bollato come nemici dell'Italia - E noi proponiamo progetti che
farebbero risparmiare letteralmente miliardi
Come irresponsabili - Mentre loro partono a trivellare l'Uranio

E tutto ciò, ovviamente, non solo sulle tasche ma anche sulla pelle della gente.

Su questi link ci sono alcuni dati significativi sulla pericolosità dell'opera.

[http://www.beppegrillo.it/immagini/Nota%20Vigili%20del%20Fuoco%20di%20Torino.pdf](http://www.beppegrillo.it/immagini/Nota%20Vigili%20del%20Fuoco%20di%20Torino.pdf)

Questo riguarda la seconda galleria, quella al fondo della valle verso Torino

[http://www.notav.it/allegati/DocUff/Gays\_amianto.pdf](http://www.notav.it/allegati/DocUff/Gays_amianto.pdf)

E ancora...

[http://www.osservatoriosullalegalita.org/05/interventi/068notavmedia.htm](http://www.osservatoriosullalegalita.org/05/interventi/068notavmedia.htm)

Ne riportiamo solo tre, e ce n'è già delle belle, ma se voleste approfondire,
sempre dal sito no tav trovate numeroso altro materiale riguardante l'impatto ambientale.

Per sapere quanto vi prendono in giro...

I soldi ci sono dicono...

[http://www.notav.it/modules.php?name=Content&pa=showpage&pid=5](http://www.notav.it/modules.php?name=Content&pa=showpage&pid=5)

L'opera è fondamentale...

[http://www.notav.it/modules.php?name=Content&pa=showpage&pid=3](http://www.notav.it/modules.php?name=Content&pa=showpage&pid=3)

Gli anni di studi e proteste taciuti!!![http://www.lunanuova.it/servizi/tav/index.html](http://www.lunanuova.it/servizi/tav/index.html)

Le manifestazioni prontamente "ridipinte" dalla stampa nazionale

[http://www.lavalsusa.com/giornale/2005-11-16\_tav/Manifestazione%20del%2016-11-2005.htm](http://www.lavalsusa.com/giornale/2005-11-16_tav/Manifestazione%20del%2016-11-2005.htm)

E infine l'appello all'Italia che lanciamo per i week end [http://www.notav.it/modules.php?name=News&amp;file=article&sid=1263](http://www.notav.it/modules.php?name=News&file=article&sid=1263)

E il link al sito ufficiale del nostro movimento [http://www.notav.it](http://www.notav.it)

su questo sito ci sono centinaia di informazioni, documenti ufficiali, dichiarazioni, ma soprattutto c'è l'aggiornamento in tempo reale dai presìdi, e dai luoghi in cui i manifestanti si stanno opponendo da giorni all'apertura (allo stato attuale illegale) dei cantieri.

Fondamentale inoltre anche [http://www.legambientevalsusa.it](http://www.legambientevalsusa.it)

Come vedete... ce n'è per tutti, nessuno escluso, e noi siamo qui a passare per nemici dell'Italia e a rischiar le botte in mezzo alla neve...

E ora mi si permetta un piccolo, ultimo sfogo.
Ci hanno detto che siamo violenti e siamo lì da giorni immobili in mezzo alla neve.
Ci hanno definiti sfaccendati e facciamo i turni svegli giorno e notte per
resistere ed andare a lavorare.
Ci hanno definiti nemici delle istituzioni e, mentre loro ci tradiscono, noi ci organizziamo per portare un po' di tè caldo anche a quei poveri poliziotti, finanzieri e carabinieri, che non ne possono nulla di quello che sta succedendo, ma che sono lì, a passare le notti al gelo come noi.
Vengano qui i ministri, se hanno ancora un po' di dignità, a vedere chi è il popolo no tav.
Vengano a vedere la valle, perché mi sa che non sanno neanche come è fatta, e se la vedessero capirebbero la pericolosità di quest'opera assurda.
Venga qui chi ci dice che pensiamo solo ai nostri interessi a vedere come il popolo no tav sta presenziando anche nelle proteste contro la costruzione delle altre opere a grave impatto ambientale.
Venga a leggere i messaggi di solidarietà dal Vajont, dallo Stretto, dal Mugello e da tantissime altre parti d'Italia.

Venga il ministro che ha appena parlato di rischio attentati a vedere come stanno le cose.
Venga almeno a vedere di cosa sta parlando.
Siamo soli in mezzo a una piana coperta di neve... ma chi dovremmo mai far saltare in aria.

Venga caro ministro a vedere chi c'è qua.
Vedrà gli anziani che hanno combattuto la seconda guerra mondiale girare con le loro medaglie al valore e spiegare ai giovani poliziotti che è per la libertà che hanno combattuto.

Cari ministri, voi che dite che fra di noi ci sono terroristi e che cercate qualunque appiglio per farci sgombrare. Con che coraggio date del terrorista a chi ha difeso la vostra patria?
Vi prego, inventatevi qualche scusa più credibile che improbabili attentati,
per giustificare alla gente l'assurda militarizzazione di una parte d'Italia.

E poi rispondetemi:
con che faccia vi sedete sulle vostre poltrone per buttare nel cesso i soldi di chi vi ha votato?
L'Italia è la patria di milioni di persone, non una terra di conquista, e noi,
nonostante i vostri insulti, non siamo qui per isolare l'Italia, ma per difenderla da chi vuole usarla per i propri, tristi interessi.

Ed ora un grazie a Voi per la vostra pazienza...

Spero vivamente che abbiate compreso le ragioni di questa mail.
So che è brutto ricevere posta indesiderata, ma allo stato attuale delle cose è l'unico mezzo "libero" che ci rimane.

Vi ringrazio della vostra attenzione e vi prego, vi prego, vi prego siate voi uno strumento di democrazia.
Le informazioni qui riportate sono tutte documentate e ufficiali.
Fate girare questa mail verso tutti quelli che conoscete.

E' l'unico modo che abbiamo per proporre dei progetti alternativi a questo ennesimo disastro ambientale italiano.

Grazie veramente tutto il sostegno che ci darete.