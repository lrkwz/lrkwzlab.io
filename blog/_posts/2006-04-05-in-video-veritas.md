---
layout: post
title: In video Veritas
date: '2006-04-05T18:22:00.000+02:00'
author: Luca Orlandi
lang: it-IT
tags:
modified_time: '2006-10-26T01:03:00.430+02:00'
blogger_id: tag:blogger.com,1999:blog-5600070.post-114425413498846392
blogger_orig_url: http://lrkwz.blogspot.com/2006/04/in-video-veritas.html
---

[Questa pagina](http://www.invideoveritas.tk/) è un contenitore di filmati riguardanti avvenimenti e dichiarazioni rilevanti dal punto di vista politico e civile.

Non vi troverai video faziosi: semmai molti video di una parte politica, quella che ha governato il Paese negli ultimi anni.
Non vi troverai satira, barzellette o reportage palesemente di parte, ma solo dichiarazioni ufficiali di personalità della politica italiana presi da telegiornali, programmi tv e documentari.