---
layout: post
title: Joel e il software
date: '2005-11-03T09:42:00.000+01:00'
author: Luca Orlandi
lang: it-IT
tags:
modified_time: '2006-10-26T01:02:59.341+02:00'
blogger_id: tag:blogger.com,1999:blog-5600070.post-113100733852738289
blogger_orig_url: http://lrkwz.blogspot.com/2005/11/joel-e-il-software.html
---

La traduzione italiana del libro ["Joel on software"](http://www.joelonsoftware.com/) è a tratti spaventosa: all'inizio il testo non è male, la narrazione "all'americana" scorre e l'italiano non fa acqua.
Dopo qualche decina di pagine iniziano ad affiorare parole che non sfigurerebbero se lasciate in "lingua originale" e via via arrivano frasi che deve tradurre in inglese per cercare di indovinare cosa potesse avere mai scritto Spolsky e finalmente afferrarne il senso.
Infine ecco intere pagine completamente prive di senso e qua e là degli "errori di battitura".

... Perché [Mondadori informatica](http://education.mondadori.it/Libri/SchedaLibro.asp?IdLibro=88-04-54057-5) si può permettere una simile mancanza di qualità?