---
layout: post
title: Ho cominciato a ridere
date: '2012-02-29T20:07:00.003+01:00'
author: Luca Orlandi
tags:
- Personal
modified_time: '2012-02-29T20:07:40.508+01:00'
thumbnail: http://4.bp.blogspot.com/-zDDgwrvtYZA/T0uyNqmT5-I/AAAAAAAAB3c/K930ugwqHLA/s72-c/IMGP0062.JPG+%28Case+Conflict+1%29
blogger_id: tag:blogger.com,1999:blog-5600070.post-9088633587090887745
blogger_orig_url: http://lrkwz.blogspot.com/2012/02/ho-cominciato-ridere.html
---


```txt
Ho cominciato a ridere
degli uomini severi
dei momenti storici
delle forme d'arte moderna.
Ho cominciato a ridere
dei pacchi sorpresa
dei campi di pallone
della miseria e della nobiltà.
Ho cominciato a ridere
delle vigne di Barolo
del profumo delle rotaie
dell'odore dei vagoni.
Ho cominciato a ridere
delle notizie porta a porta
della vita che si nasconde
della nobiltà della miseria.
Ho cominciato a ridere
ero stanco di piangere.
```



Dopo un [breve corteggiamento](http://sangiuexpress.wordpress.com/2011/11/30/laffondamento-del-cinastic/) virtuale la [banlieue](http://www.labanlieue.eu/1/post/2012/02/un-poeta-un-bardo-a-la-banlieue-di-san-giuliano.html) ha fatto centro anche questa volta: [Vincenzo Costantino Cinaski](http://vincos64.wordpress.com/blog/) è tornato sul luogo del delitto e ci ha deliziato con le sue parole intense. io devo dire che me la sono proprio goduda, un momento alto ed intenso; solo una cosa non ho compreso: perchè ridevano soprattutto le donne?