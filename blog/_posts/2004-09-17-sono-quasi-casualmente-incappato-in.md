---
layout: post
title: Sono quasi casualmente incappato in ...
date: '2004-09-17T13:25:00.000+02:00'
author: Luca Orlandi
lang: it-IT
tags:
modified_time: '2006-10-26T01:02:58.238+02:00'
blogger_id: tag:blogger.com,1999:blog-5600070.post-109541675350608909
blogger_orig_url: http://lrkwz.blogspot.com/2004/09/sono-quasi-casualmente-incappato-in.html
---

Sono (quasi) casualmente incappato in [Geolocation IP Address to Country State City ISP Organization by MaxMind](http://www.maxmind.com/); il progetto in sè è interessante.
Notevole inoltre il modello di "sviluppo sostenibile":
- librerie oper source
- alcuni tipi di dato gratuiti
- altre classi di dati o aggiornamenti periodici a pagamento