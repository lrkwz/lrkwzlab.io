---
layout: post
title: Eclipse task oriented development
date: '2007-02-19T11:53:00.001+01:00'
author: Luca Orlandi
tags:
modified_time: '2007-02-19T11:53:28.871+01:00'
blogger_id: tag:blogger.com,1999:blog-5600070.post-6367146425069909229
blogger_orig_url: http://lrkwz.blogspot.com/2007/02/eclipse-task-oriented-development.html
---

In questo periodo mi sto lasciando affascinare dai Collaborative Development Environment; il numero di Gennaio del Dr.Dobb's contiene tre quattro articoli molto interessanti tra cui un [editoriale di Grady Booch](http://www.drdobbs.com/dept/architect/196900222) .

Tra i tool presentati spicca ovviamemente Microsoft Visual Studio Team System per quanto concerne lo **sviluppo in ambiente Microsoft** (con edizioni dedicate a Architect, DB developer, Software developer e Tester). A mio parere storicamente a Microsoft i tool di supporto allo sviluppo riescono sempre piuttosto bene! Poi si potrebbe discutere sull'**opportunità di utilizzare Microsoft** come one-stop-shop per approvvigionarsi di "sistema operativo", "office applications" e strumenti di sviluppo ma questo è un'altro discorso.

Tornando ai CDE gli articoli di Dr.Dobb's lasciano capire che IBM sta preparando [un suo nuovo prodotto](http://redmonk.com/cote/2006/06/07/rational-jazz-soon-to-eat-more-of-its-own-dog-food-transparency/) ma non mi sembra esserci nulla disponibile ancora.

A questo proposito nel fine settimana sono inciampato su un plugin di Eclipse che non avevo visto prima: [MyLar](http://www.eclipse.org/mylar/), ha visto la versione 1.0 nel dicembre 2006 fornisce integrazione fra Eclipse, Bugzilla (e anche Jira e Trac più un connettore _generico_) e SVN.

L'iidea è quella di fornire un ambiente che porti a spostare l'attenzione dello sviluppo sul completamento di task utililzzando così il classico bug-tracker come repository dei task da completare/completati.

Una volta installato la task view di Eclipse viene sostituita/integrata da quella di MyLar; la prima cosa da fare  è creare una nuova query in modo da popolare la view con i task da completare.