---
layout: post
title: AIR based Android apps with no-runtime
date: '2013-09-16T12:04:00.004+02:00'
author: Luca Orlandi
tags:
modified_time: '2013-09-16T12:08:18.229+02:00'
blogger_id: tag:blogger.com,1999:blog-5600070.post-6703800316143274608
blogger_orig_url: http://lrkwz.blogspot.com/2013/09/air-based-android-apps-with-no-runtime.html
---

Hello everyone, I'm back to share part of my little experience in the mobile apps world: I've recently been involved into a mobile games project, I'm not doing much anyhow: just setting up a **controlled environment** which can leave us confident on what is being released to customers (... you know: things like maven, jenkins an the rest ... all those boring things visual programmers aren't used to ...).

Everything went smooth, my smart colleagues are producing very nice games targeted to all those cyber-childrens aged between 4 and 10 so lucky to handle a ios or android machine. Adobe's tools are really smart in compiling the air app into native adding AIR's runtime into it ... but you know hunger makes the best cook and apps have being stuffed with fancy music, videos and so on.

Here arise a problem: you cannot drop your apk into the play store if it's bigger than 50M unless you split it into external libraries, load them at runtime and so on.
In our case it is enough externalising AIR's runtime which seems to be possible since the store handles an [official copy of it](https://play.google.com/store/apps/details?id=com.adobe.air); since I run the adobe's sdk tool adt through [adt-maven-plugin](https://github.com/yelbota/adt-maven-plugin) I first unsuccessfully tried configuring  <span style="font-family: Courier New, Courier, monospace;"><target> <target><target>apk</target></target> </target> </span>unfortunately the build returned

```
[INFO] NOTE: The AIR SDK no longer supports packaging android applications for use with the shared Runtime.
[INFO] The application has been packaged with a captive Runtime.
```

I'm a lucky guy and [Antoine noticed](https://github.com/Antoine-Lassauzay) an environment variable which makes the job


    AIR_ANDROID_SHARED_RUNTIME=true


yep! I've dropped the statement into one of those [magic jenkins plugin](https://wiki.jenkins-ci.org/display/JENKINS/EnvInject+Plugin)