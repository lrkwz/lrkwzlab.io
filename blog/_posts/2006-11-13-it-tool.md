---
layout: post
title: '"Digg it" tool'
date: '2006-11-13T23:39:00.001+01:00'
author: Luca Orlandi
tags:
- Tech
modified_time: '2006-11-14T10:42:01.260+01:00'
blogger_id: tag:blogger.com,1999:blog-5600070.post-2482766703042094160
blogger_orig_url: http://lrkwz.blogspot.com/2006/11/it-tool.html
---

Ho lievemente modificato il _[bookmarklet](http://en.wikipedia.org/wiki/Bookmarklet)_ di [Jon Rohan](http://jonrohan.net/nucleus/index.php?itemid=4) per evitare che modifichi la pagina di partenza:

[Digg it!](javascript:void(digg=window.open('http://digg.com/submit?phase=2&url='+ window.location + '&title=' + document.title + '&bodytext=' + ((navigator.appName.indexOf('Microsoft')!= -1&&parseInt(navigator.appVersion)>=4)?document.selection.createRange().text:window.getSelection())));)

trascina questo script sulla tua "link bar" il testo selezionato e il titolo della pagina che visualizzi verranno aggiunti a [digg](http://digg.com/).

[Technorati Profile](http://www.technorati.com/claim/kxm62syt4)