---
layout: post
title: Ricevo e volentieri ripubblico ...
date: '2005-06-06T18:56:00.000+02:00'
author: Luca Orlandi
lang: it-IT
tags:
modified_time: '2006-10-26T01:02:59.956+02:00'
blogger_id: tag:blogger.com,1999:blog-5600070.post-114295184068076017
blogger_orig_url: http://lrkwz.blogspot.com/2005/06/ricevo-e-volentieri-ripubblico-molti.html
---



Ricevo e volentieri
ripubblico:

>
>
> "Molti di voi hanno conosciuto
> Victoria ed Enricoaxel.
>
>
>
> Chi non li ha conosciuti di persona, quasi sicuramente conosce la loro e
> nostra storia, una storia di 7 anni di fertilità assistita, di 11 tentativi in 3
> centri diversi una storia che oggi, con la legge vigente, non sarebbe più
> possibile!
>
>
>
> Roberta e i gemelli sono sui libri di medicina:
>
>
>
> tutte le eccezioni e gli incidenti a più bassa probabilità ci sono capitati,
> ma grazie alla medicina e alla nostra forza di volontà e di determinazione,
> soprattutto di Roberta, nonchè sicuramente alla nostra condizione economica, ci
> siamo riusciti.
>
>
>
> Un miracolo? un prodigio? io non sono così credente da poter credere ai
> miracoli ma se così fosse, grazie alla legge attuale, le probabilità di prodigio
> sono scese dal 41% al 16-18% Quante persone non possono più godere della
> felicità che noi adesso abbiamo in ogni attimo della nostra giornata,
> dell'incredulità quando ci soffermiamo a pensare ai momenti più tremendi di quei
> 7 anni.
>
>
>
> La nostra è stata una storia felice, di fecondazione assistita con tutte le
> tecniche e attenzioni preventive ora impossibili.
>
>
>
> Alla nascita dei gemelli prima e all?approvazione della legge poi, sia
> Roberta sia io ci siamo ripromessi di fare tutto il possibile per aiutare chi è
> ancora nel tunnel.
>
>
>
> Ci colpisce l?assenza di umanità di un dibattito che riguarda due aspetti
> estremamente dolorosi nella vita delle persone:
>
>
>
> la difficoltà di procreare e la tutela della salute.
>
>
>
> Assenza di umanità che abbiamo riscontrato nei pareri superficiali o ipocriti
> di chi in vita sua non ha dovuto affrontare la sofferenza di non riuscire ad
> avere un figlio proprio.
>
>
>
> I fatti che rendono questa legge inadeguata sono abbastanza evidenti, anche
> se probabilmente poco conosciuti da chi non ne è stato direttamente colpito e
> questo dà a molti un fortissimo alibi per non affrontare l'argomento (perfino
> una delle nostre nonne ha il coraggio di dire che non è un suo problema, che
> alla sua età è un problema che non la riguarda!)
>
>
>
>
>
> Tutto ruota intorno ad una disputa etico-teologica che non dovrebbe avere
> spazio in una legislazione nazionale.
>
>
>
> Anche per noi l?embrione è vita, ma sappiamo che ad ogni embrione non
> corrisponde una vita perchè, come ci ha spiegato uno specialista, cattolico e
> antiabortista, in natura lo spreco di embrioni è immenso.
>
>
>
> Anche chi concepisce naturalmente ha sprecato un numero enorme di embrioni,
> solo che non lo sa, non se n'è neanche accorto/a.
>
>
>
> La nostra storia fortunata, per esempio, è fatta di 12 embrioni, tutti
> impiantati in diversi momenti:
>
>
>
> solo gli ultimi due, gli unici rimasti prima di dover ricominciare daccapo
> tutta la trafila di stimolazione, solo due, quando normalmente se ne impiantono
> 3-4, sono diventati vita.
>
>
>
> Anche altri prima erano stati impiantati, ma con buona pace di chi sostiene
> che sono già vita, si sono "suicidati" fra la 9a e la 12a settimana... no basta
> l'impianto per farli nascere!
>
>
>
> Enricoaxel e Victoria hanno tenuto Roberta a letto 5 mesi, sotto cura
> intensiva di iniezioni per rafforzare il loro debole attaccamento alla "vita"
> che non è ancora vita...
>
>
>
>
>
> Noi voteremo sì a tutti i quesiti e ci piacerebbe scoprire che quest nostre
> parole vi hanno aiutato, innanzitutto a non ignorare la faccenda, poi ad andare
> a votare e, magari, a votare Sì anche voi, Sì a tutti e 4 i quesiti. Perchè?
>
>
>
> Perchè per produrre solo tre embrioni bisogna limitare l?utilizzo degli
> ovociti che si hanno a disposizione (secondo la legge produrne anche solo uno in
> più è reato).
>
>
>
> Questo significa che mentre prima i biologi cercando di fecondare tutti gli
> ovociti ottenuti con la stimolazione (ovviamente ad ogni ovocita fecondato non è
> detto che corrisponda automaticamente un embrione vivo!) avevano una maggiore
> possibilità di ottenere embrioni forti e con buona probabilità di vita, adesso
> questa possibilità si è notevolmente ridotta.
>
>
>
> Perchè impedire le analisi sugli embrioni prima dell'impianto? Perchè
> costringere le donne e le coppie alla amniocentesi o all'analisi dei villi
> quando buona parte di quei problemi si possono diagnosticare prima?
>
>
>
> Perchè per un principio di vita sull'embrione, formulare una legge che
> inevitabilmente favorisce l'aborto in quelle coppie che se riscontrato un
> problerma sul feto decidono di abortire? perchè non prevenire questo "omicidio"
> consnetendo la scelta degli embrioni prima che siano vitali?
>
>
>
>
>
> Perchè impedire che gli embrioni vengano congelati significa imporre alle
> donne un numero superiore di stimolazioni ovariche.
>
>
>
> Sapete cosa significa? Sapete quante iniezioni al giorno sono necessarie?
>
>
>
> Sapete cosa significa non trovare più un punto in cui fare l'iniezione?
>
>
>
> sapete cosa significa fare impacchi di alcool e lasonil per ore per cerare di
> ridurre i ponfi e far assorbire il liquido oleoso prima delle ore oltre le quali
> perde efficacia? Sapete che effetti devastanti hanno gli ormoni sulla donna sia
> a livello fisico sia mentale?
>
>
>
> E' una tecnica faticosa per chi la subisce e costosissima per il servizio
> sanitario nazionale (se riesci ad usufruirne!.) Ogni scatola di ormoni per la
> fecondazione costa circa 100 Euro e per ogni ciclo mensile se ne comprano almeno
> un paio, ma nei casi più difficili si arriva ad usarne 1 e mezza al giorno...
>
>
>
> E poche donne riescono a fare più di 2-3 ovociti alla prima stimolazione!
>
>
>
> A volte ce ne vogliono 3-4!
>
>
>
> Perché obbligare una donna ad impiantare tutti gli embrioni ottenuti, salvo
> poi concederle un aborto terapeutico, è una violenza che può essere paragonata
> allo stupro.
>
>
>
> Perchè obbligare una donna ad un impianto che per qualche motivo, magari
> terapeutico per problemi subentrati dopo la stimolaione, non vuole più perchè
> magari corre grossi rischi di salute o di vita... cosa faranno, la prendono e la
> legano per fare l'impianto comunque? Come starà la nostra coscienza sapendo che
> ci spotranno essere concepimenti così?
>
>
>
> Perché impedire la ricerca sugli embrioni, che fino al 10o giorno sono un
> insieme di cellule a-specialistiche ossia non ancora destinate a diventare
> questo o quell'organo?
>
>
>
> Perchè impedire che queste cellule, ancora senza terminali nervosi,
> altamanete instabili e assolutamente incapaci di una vita propria, cellule che
> in buona parte spesso vanno comunque perse, perchè impedire di usarele per
> curare malattie come il parkinson o l'alzheimer? Non vi pare una crudeltà
> ingiustificata nei confronti dei malati?
>
>
>
> E comunque supponendo per un attimo di impedirlo per il futuro, perchè
> buttare via le centinaia di migliaia di embrioni attualmente surgelati da parte
> di coppie che non li useranno più e hanno spontaneamente scelto di renderli
> disponibili, di donarli per la medicina?
>
>
>
> Perché impedire la fecondazione eterologa? è una limitazione della libertà
> individuale inaccettabile.
>
>
>
> Lasciamo ai singoli di decidere se accettare un ovocita o uno spermatozoo
> proveniente da fuori la coppia!
>
>
>
> Quanti re e regine e dinastie sono state salvate da donazioni eterologhe
> fornite da scudieri e ufficiali di accompagnamento?
>
>
>
> Perchè una donzione naturale si può sanare con una confessione più o meno
> pentita o semplicemente non dicendo niente e invece una eterologa, controllata,
> gratuita, scientificaemte guidata, va impedita?
>
>
>
> Senza offesa per nessuno ma siamo tutti così sicuri? matere semper certa est
> dicevano i latini...
>
>
>
> Per i credenti, ricordiamoci che perfino la Madonna ha usufruito di una
> donazione esterna alla coppia!
>
>
>
>
>
> Non ponetevi la domanda: "Io se fossi in tali condizioni, la farei?"
>
>
>
> Non fatelo! E' una domanda ipocrita e irresponsabile, se in tali condizioni
> non vi siete trovati! Non potete sapere come reagireste.
>
>
>
> Non appellatevi nenache all'affermazione "Perchè accanirsi quando c'è la
> possibilità dell'adozione?"
>
>
>
> A chi propone l?adozione come alternativa, posso solo raccontare l'
>
>
>
> esperienza diretta, nostra e di tante altre coppie:
>
>
>
> molto spesso la situazione si sblocca e l'impianto riesce dopo aver
> depositato la domanda d'azione.
>
>
>
> Noi l'avevamo fatto in gennaio 99 e andammo ai colloqui a luglio e settembre
> (sì perchè a Milano ti chiamano 7-8 mesi dopo la domanda!) con Roberta e il suo
> pancione: vi lascio immaginare le facce e i commenti degli operatori.
>
>
>
> sentirsi dare degli irresponsabili piuttosto di quelli che vogliono togliere
> una pssibilità ad altri pur essendo "incinti"...
>
>
>
> Chi suggersisce e caldeggia l'adozione, guarda caso quasi sempre sono persone
> single o che di figli ne hanno già, quasi sempre biologici, non adottati!
>
>
>
> Questa è una legge la cui ratio e disciplina di applicazione sono - per
> quanto in un ambito limitato - crudeli, inconsistenti e autoritarie.
>
>
>
> Crudeli, perché manovrano senza pietà il destino altrui.
>
>
>
> Inconsistenti, perché ostacolano i processi con regole da burocrati (massimo
> tre embrioni) senza affrontare i principi di base (e l?aborto?).
>
>
>
> Autoritarie, per non dire fasciste che oramai non si usa più, perché riducono
> senza ragioni di Stato la libertà individuale.
>
>
>
> Per questi motivi vi chiediamo di VOTARE SI' a tutti i quesiti del
> referendum.
>
>
>
> Per questi motivi vi chiediamo almeno di andare a votare, magari scheda
> bianca, ma votare.
>
>
>
> Con il nostro voto, e con la nostra partecipazione, faremo una scelta di
> umanità e di libertà.
>
>
>
> Se volete saperne di più contattate [Emanuele](mailto:emanuele.kettliz@rcm.inet.it): saremo ben
> contenti di aiutarvi a fare una scelta... qualunque essa sia, ma vostra,
> consapevole, civile, senza ipocrisie e false ideologie, magari altrui.
>
>
>
> Grazie della pazienza, buon voto
>
>
>
> La tribù dei kettlitz. Emanuele, Roberta, Enricoaxel e Victoria (forse adesso
> capite perchè si chiama così!)"