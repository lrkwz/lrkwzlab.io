---
layout: post
title: Magento trapped me again
date: '2016-01-15T16:45:00.002+01:00'
author: Luca Orlandi
lang: en-US
tags:
- php
- magento
modified_time: '2016-01-15T19:17:05.751+01:00'
blogger_id: tag:blogger.com,1999:blog-5600070.post-8068837004754399444
blogger_orig_url: http://lrkwz.blogspot.com/2016/01/magento-trapped-me-again.html
---

It isn't the first time ... refactoring a module (well actually I'm transforming and extensions which was incompatible with another installed one) I realized that only the admin panels were working, the product page nearly disappeared: header and
footer plus sidebar but no product contents at all.<br/><br/>After validating all the mappings, blocks etc etc it ended up that I accidentally reformat the config.xml ... a new-line was dropped after the class name !<br/><br/>

```xml
<catalog>
  <rewrite>
    <product_view_options_type_select>Companyname_Modulename_Block_Product_View_Options_Type_Select
    </product_view_options_type_select>
  </rewrite>
</catalog>
```

Magento's XML parser fails to match the class name resulting in the empty page.<br/><br/>
