---
layout: post
title: Ricerca personalizzata?
date: '2006-10-02T00:27:00.000+02:00'
author: Luca Orlandi
tags:
modified_time: '2006-10-26T01:03:01.298+02:00'
blogger_id: tag:blogger.com,1999:blog-5600070.post-115974167631213273
blogger_orig_url: http://lrkwz.blogspot.com/2006/10/ricerca-personalizzata.html
---

[](http://photos1.blogger.com/blogger/8036/83/1600/sshot-2.gif)

[Technorati tags:](http://photos1.blogger.com/blogger/8036/83/1600/sshot-2.gif) [privacy](http://technorati.com/tags/privacy), [tracking](http://technorati.com/tags/tracking), [google](http://technorati.com/tags/google)

![](http://photos1.blogger.com/blogger/8036/83/320/sshot-2.png)
[![](http://photos1.blogger.com/blogger/8036/83/320/sshot-1.png)](http://photos1.blogger.com/blogger/8036/83/1600/sshot-1.png)

Questa ancora non la avevo (ancora) vista; non avevo mai fatto caso al comando "Turn ON/OFF personalized search".

Adesso che lo ho visto, preso dalla curiosità sono riuscito a trovare la prima **traccia** delle mie ricerche ...




e non uso "traccia" a caso.