---
layout: post
title: Al telefono da mumbai
date: '2008-11-26T23:41:00.002+01:00'
author: Luca Orlandi
tags:
- Personal
modified_time: '2011-09-14T22:05:43.333+02:00'
blogger_id: tag:blogger.com,1999:blog-5600070.post-1896851478603171982
blogger_orig_url: http://lrkwz.blogspot.com/2008/11/al-telefono-da-mumbai-seguendo-quel-che.html
---

Seguendo quel che accade in questo momento a Mumbai e sono "cascato" su un webcast a mio avviso molto interessante: [blogtalkradio](http://www.blogtalkradio.com/) ospita uno [spazio gestito da una associazione di giornalisti asiatici](http://www.blogtalkradio.com/saja) che trasmette in diretta delle telefonate alcune delle quali da Mumbai; è possibile chiamare un numero di NY oppure richiedere la chiamata dal proprio PC.

Un'altra fonte facilmente accessigile è lo [streaming video convertito in flash sul sito della CNN](http://edition.cnn.com/video/flashLive/live.html?stream=1) (probabilmente lo streaming è sotto carico in questo istante e quindi poco fruibile, lo streaming flash probabilmente è meno utilizzato).