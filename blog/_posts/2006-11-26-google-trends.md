---
layout: post
title: Google Trends
date: '2006-11-26T00:06:00.001+01:00'
author: Luca Orlandi
tags:
modified_time: '2006-11-28T01:04:18.269+01:00'
blogger_id: tag:blogger.com,1999:blog-5600070.post-7143574549701826382
blogger_orig_url: http://lrkwz.blogspot.com/2006/11/google-trends.html
---

Dalla fine del 2005 abbiamo a disposizione un servizio che mostra l'andamento di ciascuna delle nostre ricerche personali

[![](http://photos1.blogger.com/x/blogger2/4934/683/320/523719/ScreenShot001.jpg)](http://photos1.blogger.com/x/blogger2/4934/683/1600/37376/ScreenShot001.jpg)



Più recentemente, all'inizio di ottobre l'annuncio di [un servizio](%3Ca%20href=%22http://www.google.com/trends?q=boccioni%2C+lempicka&ctab=0&amp;geo=all&date=all%22%3E) per l'analisi e il confronto dei dati statistici delle ricerche (ovviamente profilate ma questo è un'altro discorso):

[![](http://photos1.blogger.com/x/blogger2/4934/683/320/116769/ScreenShot002.jpg)](http://photos1.blogger.com/x/blogger2/4934/683/1600/22199/ScreenShot002.jpg)

la curva delle richieste può essere confrontata con i picchi di frequenza dell'occorrenza all'interno delle news delle keyword analizzate.

Le eventuali news vengono evidenziate e mostrate in corrispondenza di ciscun picco.

Di cosa si tratta? Per esempio di un servizio di marketing per "google l'editore pubblicitario" che fa "mostra i muscoli" della profilazione.

Oppure di un servizio a supporto del marketing pubblicitario per individuare le parole chiave (da acquistare) più efficaci e per capire come pilotare il copy delle veline a supporto dei lanci pubblicitari.

O anche di un servizio tramite il quale verificare l'efficacia di determinate notizie diffuse ad arte ... per carità è solo la Rete e non la vita reale ma a certa gente non dispiace portarsi avanti con gli esperimenti.