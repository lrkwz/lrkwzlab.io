---
layout: post
title: Double opt-in pattern
date: '2006-03-21T15:09:00.000+01:00'
author: Luca Orlandi
lang: it-IT
tags:
modified_time: '2006-10-26T01:02:59.599+02:00'
blogger_id: tag:blogger.com,1999:blog-5600070.post-114295016421984364
blogger_orig_url: http://lrkwz.blogspot.com/2006/03/double-opt-in-pattern.html
---

Mi sono messo alla ricerca di una definizione formale del pattern di double opt-in senza alcun successo.

Nel frattempo ho individuato un interessante post di James Tauber su "[Account Management Patterns](http://jtauber.com/blog/2006/03/20/account_management_patterns)" con una schematizzazione molto chiara dei meccanismi di autenticazione e una dissertazione sull'uso del GET per l'opt-in ("[Don't Let Users Confirm Via HTTP GET](http://www.artima.com/weblogs/viewpost.jsp?thread=152805)").