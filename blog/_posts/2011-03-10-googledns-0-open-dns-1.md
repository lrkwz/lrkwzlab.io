---
layout: post
title: Google DNS 12.000 - Open DNS 800
date: '2011-03-10T12:14:00.003+01:00'
author: Luca Orlandi
tags:
- Tech
modified_time: '2011-09-14T22:04:50.285+02:00'
thumbnail: https://lh3.googleusercontent.com/-T81ZpKZCxjg/TXitbFBOZ0I/AAAAAAAABlI/hX_yDVxqF18/s72-c/Screenshot-3.png
blogger_id: tag:blogger.com,1999:blog-5600070.post-1022585823982039850
blogger_orig_url: http://lrkwz.blogspot.com/2011/03/googledns-0-open-dns-1.html
---

La velocità è importante, oltre a preoccuparmene nella progettazione dei sistemi per i miei clienti cerco anche di velocizzare quanto possibile la mia personale navigazione.

Ho fatto qualche test e sicuramente i due DNS (8.8.8.8 e 8.8.4.4 ... ma come li hanno presi due IP così _belli_?) [messi a disposizione da Google](http://code.google.com/speed/public-dns/) sono i più veloci .. ma ... sì c'è un ma ... il fatto che risolvano i nomi più velocemente [degli altri](http://www.opendns.com/) (208.67.222.222, 208.67.220.220, ...) non comporta necessariamente un vantaggio.

Ad esempio io consiglio spesso di utilizzare servizi di [content delivery network](http://it.wikipedia.org/wiki/Content_Delivery_Network) (p.e. [Akamai](http://www.akamai.com/) oppure quello di [amazon web services](http://aws.amazon.com/)), questi si basano sulla risoluzione geolocalizzata dei nomi per dirigere il browser verso il proxy geograficamente più vicino all'utente. Putrroppo la risoluzione è fatta a livello di DNS quindi non tiene conto della posizione geografica dell'ip dell'utente. In poche parole se accedi per esempio a www.kinder.com usando il DNS di google otterrai l'IP di un server akamai in california (!) quindi i contenuti dovranno attraversare la dorsale atlantica prima di arrivare a casa tua.

Va meglio se invece usi [OpenDNS](http://www.opendns.com/);  segue dimostrazione:

[![](https://lh3.googleusercontent.com/-T81ZpKZCxjg/TXitbFBOZ0I/AAAAAAAABlI/hX_yDVxqF18/s320/Screenshot-3.png)](https://lh3.googleusercontent.com/-T81ZpKZCxjg/TXitbFBOZ0I/AAAAAAAABlI/hX_yDVxqF18/s1600/Screenshot-3.png)questo screenshot mostra l'outpt di dig @8.8.8.8 www.kinder.com da cui si vede che veniamo indirizzati su due macchine dell'edgesuite di akamai (due perchè in caso una dovesse essere in manutenzione c'è l'altra a disposizione ... gioia dell'high availability!).

Utilizzando [visual trace-route](http://www.yougetsignal.com/tools/visual-tracert/) puoi verificare dove essettivamente si trovino quelle due macchine e in questo caso ce ne andiamo dritti sparati sulla costa atlantica i contenuti statici del sito devono percorrere circa 12.000 km

[![](https://lh6.googleusercontent.com/-la72crLYNEU/TXiv40sKtJI/AAAAAAAABlM/u5AJDPY9sJU/s320/Screenshot-5.png)](https://lh6.googleusercontent.com/-la72crLYNEU/TXiv40sKtJI/AAAAAAAABlM/u5AJDPY9sJU/s1600/Screenshot-5.png)



[![](https://lh5.googleusercontent.com/-rt2otURH2sk/TXiwZxR9SzI/AAAAAAAABlQ/7hhEQWhps4M/s320/Screenshot-4.png)](https://lh5.googleusercontent.com/-rt2otURH2sk/TXiwZxR9SzI/AAAAAAAABlQ/7hhEQWhps4M/s1600/Screenshot-4.png)

Le cose funzionano meglio usando OpenDNS infatti dig mostra che il medesimo host viene risolto con un ip diverso e questo corrisponde ad una macchina che si trova in olanda

[![](https://lh6.googleusercontent.com/-0VUbnc2BcZU/TXixFPkpDTI/AAAAAAAABlU/Prr8YjRJla8/s320/Screenshot-2.png)](https://lh6.googleusercontent.com/-0VUbnc2BcZU/TXixFPkpDTI/AAAAAAAABlU/Prr8YjRJla8/s1600/Screenshot-2.png)













Quasi dimenticavo di dire che ho deciso di usare dei DNS pubblici anche perchè quelli normalmente indicati dal mio isp (del quale ometto il nome ma i più attenti non faranno fatica a rendersi conto di chi si tratta) spesso si inchiodano e il risultato è che non riesco a navigare fintanto che non riavvio il modem adsl e a questo viene assegnato un diverso DNS.