---
layout: post
title: JBoss e Eclipse
date: '2006-11-10T17:41:00.001+01:00'
author: Luca Orlandi
tags:
- Tech
modified_time: '2006-11-27T17:19:54.939+01:00'
blogger_id: tag:blogger.com,1999:blog-5600070.post-8904007704593611543
blogger_orig_url: http://lrkwz.blogspot.com/2006/11/jboss-e-eclipse.html
---

<p>Lavorando spesso su diversi progetti nel medesimo lasso di tempo, quando uso  jboss di norma copio il contenuto della directory $JBOSS_HOME/servers/default in  una directory "<em>di progetto</em>"; in questo modo la configurazione per uno  specifoco progetto non inquina gli altri.</p> <p>Purtroppo quando configuri Eclipse per eseguire il server jboss l'elenco  prevede i soli server "standard": <em>all</em>, <em>default</em>  e <em>minimal.</em></p> <p>Per aggiungere un server extra senza andare ad agire sul parametro di  esecuzione puoi modificare il file</p><p></p><blockquote><p>  $ECLIPSE_HOME/plugins/org.eclipse.jst.server.generic.jboss_1.5.1.v200609140551/servers/jboss.serverdef</p><p></p></blockquote><p>  (ovviamente il numero di revision del plugin può variare).</p>