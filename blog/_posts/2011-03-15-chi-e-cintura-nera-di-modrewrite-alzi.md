---
layout: post
title: Chi è cintura nera di (mod_)rewrite alzi la mano
date: '2011-03-15T12:49:00.000+01:00'
author: Luca Orlandi
tags:
- apache
modified_time: '2011-09-14T22:03:56.658+02:00'
blogger_id: tag:blogger.com,1999:blog-5600070.post-4459519518484171355
blogger_orig_url: http://lrkwz.blogspot.com/2011/03/chi-e-cintura-nera-di-modrewrite-alzi.html
---

L'url rewriting in generale è una tecnica molto importante per disaccoppiare gli url delle request e l'applicativo che sostiene un sito. Io utilizzo [tuckey](http://www.tuckey.org/urlrewrite/) al livello di webapp e mod\_rewrite a livello di http server.
Il modulo di rewrite di apache in particolare è un portento ma è un poco ostico da debuggare fintanto che non utilizzi

```
<IfModule mod_rewrite.c>
  RewriteLog "logs/rewrite.log"
  RewriteLogLevel 3
<IfModule\>
```
L'ultima rewrite che ho scritto serve a ridirigere il browser su un url che contiene esplicitamente il locale di navigazione in modo che i crawler (... si è sempre il seo a creare problemi ...) indicizzino correttamente per lingua:

```
RewriteCond %{HTTP\_COOKIE} locale=(\[^;\]+) \[NC\]
RewriteRule ^/$ /site/%1 \[R=301\]
```
Questa volta le fonti sono

1.  [Fresh .htaccess Examples: Cookies, Variables, Custom Headers](http://www.askapache.com/htaccess/htaccess-fresh.html "Fresh .htaccess Examples: Cookies, Variables, Custom Headers")
2.  [A Couple Ways to Debug mod\_rewrite](http://www.latenightpc.com/blog/archives/2007/09/05/a-couple-ways-to-debug-mod_rewrite)