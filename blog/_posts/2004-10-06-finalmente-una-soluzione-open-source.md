---
layout: post
title: Mount crypted file
date: '2004-10-06T09:47:00.000+02:00'
author: Luca Orlandi
lang: it-IT
tags:
modified_time: '2006-10-26T01:02:58.627+02:00'
blogger_id: tag:blogger.com,1999:blog-5600070.post-109704887614671180
blogger_orig_url: http://lrkwz.blogspot.com/2004/10/finalmente-una-soluzione-open-source.html
---

Finalmente una soluzione open source per cryptare i file e montarli come se fossero un disco: [TrueCrypt - Free Open-Source On-The-Fly Encryption for Windows XP/2000/2003](http://truecrypt.sourceforge.net/)