---
layout: post
title: Odissea
date: '2007-12-11T11:56:00.000+01:00'
author: Luca Orlandi
tags:
- Personal
modified_time: '2011-09-14T22:06:14.637+02:00'
blogger_id: tag:blogger.com,1999:blog-5600070.post-9082111762952857049
blogger_orig_url: http://lrkwz.blogspot.com/2007/12/odissea-doveva-essere-uno-sbattimento.html
---

Odissea
Doveva essere uno sbattimento, un semplice sbattimento da Milano Linate a Parigi con ritorno in giornata.

Ore 12:00 il volo viene annullato, senza colpo ferire siamo caricati su un pullman alla volta di Malpensa; la scusa ufficiale è la nebbia, sul pullman ci contiamo; siamo in 30 nulla mi toglie dalla testa che fare volare un aereo con 30 persone non conviene quindi ne mettiamo insieme due poco male se i passeggeri perdono un poco di tempo.

Fin qui tutto lineare anzi con il supporto di una partita a mastermind via sms il tempo passa facile. Ovviamente il volo delle 13;10 non è raggiungibile, fra me e lui si frappone una coda epica ai controlli di sicurezza ... si sa il personale è a pranzo.

L'aereo delle 14 arriva dalla destinazione precedente con tre quarti d'ora di ritardo ma saliamo tutti a bordo, tutti meno il signor Abu Omar. Il capitano lo chiama, tutti lo cercano ma il signor Abu non c'è, i suoi bagagli invece si. Il personali li cerca, li trova e infine li scarica. Sarà contento il signor Abu e sicuramente sono più sollevate le sciure che all'annuncio del capitano hanno sentito il brivido del terrorista islamico.

Finalmente atterriamo a Parigi, ho solo il bagagli o a mano quindi lesto lesto mi accingo a guadagnare l'uscita. La dogana è chiusa ... neanche un poliziotto in giro, sono oramai le 17 passate, finalmente compare un giovanotto robusto con una maglia da rugby e la fascia rossa che lascia capire che è della security: qualcuno ha dimenticato un bagaglio fuori dall'aeroporto ... pazientemente aspettiamo che gli artificieri si portino via il prezioso pacco.

Congrua coda per il taxi e finalmente dopo essermi spiegato con il mio francese stentoreo mi svacco in un lussuosissimo mercedes, musica jazz di sottofondo. Dopo 2km il tassista nero come la sua giacca ... più adatto ad un club che ad un taxi mi chiede se può fare gasolio, come dirgli di no oramai, solo ancora un incidente in autostrada ed eccomi arrivato.

La mia riunione si prolunga un poco e perdo il volo del rientro, ce ne è un altro, per raggiungerlo in tempo balzo in sella ad un moto taxi, casco con interfono e musica, giacca e guanti professionali copertina tucano, sedile riscaldato ...cazzo ci sono 6 gradi ma qui si cuoce! comunque sia malgrado la temperature sfrecciamo nel traffico del rientro e raggiungiamo l'aeroporto charles de gaule appena in tempo ... appena in tempo per aspettare 55 minuti sulla pista che il nostro slot su Malpensa si liberi: c'è la nebbia e quindi Malpensa riceve ma riceve meno.

Si è fatta mezzanotte, tra 25 minuti parte la navetta che mi riporta a Linate dove ho lasciato la moto.

Già dimenticavo, la giornata è fredda e di nebbia ce ne è pochina ,,, figuriamoci quando c'è il nebbione!