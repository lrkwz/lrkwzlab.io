---
layout: post
title: jCryption for Spring
date: '2011-09-14T20:51:00.000+02:00'
author: Luca Orlandi
lang: it-IT
tags:
- spring
- Tech
- crypting
- java
modified_time: '2011-09-27T14:47:01.236+02:00'
thumbnail: http://3.bp.blogspot.com/-A56HfJtGviI/TncwA4KVJXI/AAAAAAAABm4/6LNA_k815Rw/s72-c/json.png
blogger_id: tag:blogger.com,1999:blog-5600070.post-3213540699072380291
blogger_orig_url: http://lrkwz.blogspot.com/2011/09/jcryption-for-spring.html
---

Mi è capitato diverse volte che un cliente mi richiedesse di implementare una forma di criptazione dei dati POSTati dalle form senza utilizzare SSL. Mi sono ripromesso di soprassedere sulla opportunità di questo requisito e sulle effettive caratteristiche di sicurezza di una soluzione basata sul crypting client side in javascript e provvedere ad una implementazione il più possibile solida e semplice.

Dopo diverse ricerche sono riuscito ad individuare
<a href="http://www.jcryption.org/">jCryption</a>, un piccolo framework javascript/PHP appoggiato su
<a href="http://jquery.com/">jQuery</a>.


Dato che le applicazioni che realizzo sono normalmente in java basate su
<a href="http://www.springframework.com/developer/spring">sprigframework</a>
ho messo assieme un modulo di integrazione ragionevolmente semplice da utilizzare.

Gli obiettivi di questo modulo sono:

* evitare l'uso della componente PHP
* evitare di inserire il codice di decrypting all'interno del codice applicativo (decoupling)
* no java coding

Il sistema nel suo complesso funziona in questa maniera:

![JSON call result](../assets/img/jcryption/json.png)

* il client al momento della POST chiama un controller che rende (*) la chiave pubblica di codifica
* la libreria javascript crypta tutti i campi della form in una unica stringa di testo che viene POSTata al controller
* un filtro intercetta le chiamate che contengono nel payload la variabile cryptata la decripta&nbsp; e wrappa la richiesta http in modo da servire le medesime variabili della POST originale
* a questo punto la catena dei filtri prosegue come al solito

![Parametro cryptato](../assets/img/jcryption/post.png)


(*) La definizione del controller JSON è resa banale dal Spring 3.x: se non lo hai già fatto leggi "<a href="http://blog.springsource.com/2010/01/25/ajax-simplifications-in-spring-3-0/">Ajax Simplifications in Spring 3.0</a>" ... in poche parole è sufficiente aggiungere una dipendenza al pom.xml e annotare correttamente il controller ... spring è insuperabile per questo!


In questo modo è per esempio possibile realizzare una applicazione standard inserendo in pochi punti quanto serve la il crypting; proviamo a farlo passo passo con l'ausilio di
<a href="http://www.springsource.org/roo">roo</a>:

esegui i seguenti comandi nella shell di roo ini modo da inizializzare una web application che consenta di creare e modificare un oggetto di tipo UserProfile con alcune proprietà

```
project --topLevelPackage org.gitorious.jcryptionspring.sample
persistence setup --database HYPERSONIC_IN_MEMORY --provider HIBERNATE
entity --class ~domain.UserProfile
field string --class ~domain.UserProfile --fieldName email
field string --class ~domain.UserProfile --fieldName name
field string --class ~domain.UserProfile --fieldName surname
field date --class ~domain.UserProfile --fieldName birthday --type java.util.Calendar
field boolean --class ~domain.UserProfile --fieldName enabled
logging setup --level DEBUG
controller scaffold --class ~.web.UserProfileController
security setup
dependency add --artifactId jcryption-spring --groupId org.gitorious.jcryptionspring --version 0.1.1
quit
```

lo script precedente aggiunge al `pom.xml` la dipendenza dal mio modulo di integrazione

```
<dependency>
            <groupId>org.gitorious.jcryptionspring</groupId>
            <artifactId>jcryption-spring</artifactId>
            <version>0.1.2</version>
</dependency>
```

a questo punto è necessario aggiungere le librerie javascript che possono essere
<a href="http://code.google.com/p/jcryption/downloads/">scaricate dal sito di jCryption</a>
(o dai sorgenti su
<a href="https://github.com/HazAT/jCryption">github</a>).

Modifica il file `src/main/webapp/WEB-INF/layouts/default.jspx` in modo che le librerie js vengano incluse in ogni pagina

```
<spring:url var="home" value="/" />
<script src="${home }js/jquery-1.4.2.min.js" type="text/javascript"></script>
<script src="${home }js/jquery-ui-1.8.2.custom.min.js" type="text/javascript"></script>
<script type="text/javascript" src="${home }js/security/jquery.jcryption-1.2.js"></script>
```

<br/>A questo punto bisogna agganciare la funzione di crypting alla post di ciascuna form (p.e. `src/main/webapp/WEB-INF/views/userprofiles/create.jspx` e `src/main/webapp/WEB-INF/views/userprofiles/update.jspx`) inserendo :

```
<spring:url value="/EncryptionServlet?generateKeypair=true"  var="getKeysURL"/>
<script>
/*<![CDATA[ */

$(function() {

$("#userProfile").jCryption({getKeysURL:"${getKeysURL}"});

});
/*]]>*/
</script>
```

per fare in modo che la chiamata ```/EncryptionServlet``` abbia luogo puoi aggiungere alla configurazione mvc `src/main/webapp/WEB-INF/spring/webmvc-config.xml`:

```
<context:component-scan base-package="org.gitorious.jcryptionspring">
</context:component-scan>
```



infine è necessario configurare il filtro di decripting nel `src/main/webapp/WEB-INF/web.xml` inserendo

```
<filter>
    <filter-name>DecryptParameters</filter-name>
    <filter-class>org.gitorious.jcryptionspring.ParameterDecryptingFilter</filter-class>
</filter>
<filter-mapping>
    <filter-name>DecryptParameters</filter-name>
    <url-pattern>/*</url-pattern>
</filter-mapping>
```

prima del `CharacterEncodingFilter` in maniera che l'ordine dei filtri resti inalterato (lo Spring `OpenEntityManagerInViewFilter` deve rimanere al suo posto per non incorrere nel fatidico "
<a href="http://www.google.com/search?client=ubuntu&amp;channel=fs&amp;q=%3Cfilter%3E%3Cfilter-name%3EDecryptParameters%3C%2Ffilter-name%3E%3Cfilter-class%3Eorg.gitorious.jcryptionspring.ParameterDecryptingFilter%3C%2Ffilter-class%3E%3C%2Ffilter%3E%3Cfilter-mapping%3E%3Cfilter-name%3EDecryptParameters%3C%2Ffilter-name%3E%3Curl-pattern%3E%2F*%3C%2Furl-pattern%3E%3C%2Ffilter-mapping%3E&amp;ie=utf-8&amp;oe=utf-8#sclient=psy-ab&amp;hl=en&amp;client=ubuntu&amp;hs=bDY&amp;channel=fs&amp;source=hp&amp;q=detached+entity+passed+to+persist&amp;pbx=1&amp;oq=detached+entity+passed+to+persist">Detached entry passed to persist</a>"

Semplice no?

Il package compilato è disponibile sul repository central di maven e i sorgenti (compreso un esempio funzionante) su
<a href="http://gitorious.org/jcryptingspring">gitorious</a>
