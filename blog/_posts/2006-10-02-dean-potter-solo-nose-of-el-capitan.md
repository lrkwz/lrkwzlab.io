---
layout: post
title: Dean Potter [solo] The Nose of El Capitan [Yosemite Valley]
date: '2006-10-02T00:40:00.000+02:00'
author: Luca Orlandi
tags: 
modified_time: '2006-10-26T01:03:01.370+02:00'
blogger_id: tag:blogger.com,1999:blog-5600070.post-115974278854278382
blogger_orig_url: http://lrkwz.blogspot.com/2006/10/dean-potter-solo-nose-of-el-capitan.html
---

<table xmlns="http://purl.org/atom/ns#" border="0" cellpadding="0" cellspacing="0"><tr><td colspan="2"><embed id="VideoPlayback" src="http://video.google.com/googleplayer.swf?docId=-4832527121139830698&amp;hl=en" style="width:300px; height:243px;" type="application/x-shockwave-flash"> </embed></td></tr><tr/><tr><td>"Dean Potter soloing the Nose of El Capitan in Yosemite Valley" è il secondo video di arrampicata libera che vedo che mi colpisce molto; il video in sè è ben fatto, l'arrampicata non è certo pulita ma sicuramente emozionante!</td></tr></table>