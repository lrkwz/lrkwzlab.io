---
layout: post
title: Sopravvivere a Micro$oft Project
date: '2007-01-30T23:12:00.001+01:00'
author: Luca Orlandi
tags:
modified_time: '2008-12-11T12:19:14.134+01:00'
thumbnail: http://2.bp.blogspot.com/_RjgCgazXb04/Rb_DWASYoYI/AAAAAAAAAAM/Q8gWMxngcjk/s72-c/clip_image002.jpg
blogger_id: tag:blogger.com,1999:blog-5600070.post-3008003314169178010
blogger_orig_url: http://lrkwz.blogspot.com/2007/01/sopravvivere-microoft-project.html
---

[
](http://2.bp.blogspot.com/_RjgCgazXb04/Rb_DWASYoYI/AAAAAAAAAAM/Q8gWMxngcjk/s1600-h/clip_image002.jpg)

Premetto che non sono un utente esperto di [MSProject](http://en.wikipedia.org/wiki/Microsoft_project) e che nemmeno amo molto utilizzarlo anche perché non sono così metodico e preciso da mantenere il documento aggiornato allineandolo puntualmente con l’avanzamento degli sviluppi.

Tra le ragioni delle mie perplessità ogniqualvolta occorre l’occasione di utilizzare Project ci sono quelle che seguono:

Non è semplice mettere a fuoco lo **scopo della rappresentazione** del piano: “**_Promemoria_** _per l’ipotetica valutazione del carico di lavoro con l’obbiettivo di definire il budget/costo del progetto_” oppure “**_Contentino_** _per il cliente per dimostrare che sono in grado di terminare il progetto entro i limiti imposti/richiesti_” o invece “**_Strumento_** _di controllo di gestione del progetto_” ?.

Questi obiettivi portano generalmente a documenti con contenuti anche radicalmente differenti sia in termini di livello di dettaglio dei task elencati che di effort espressi ed in ultima analisi di date di scadenza e priorità.

**Costa** (molto) **tempo e** (molta) **fatica** impostare un piano:

*   Sufficientemente completo per potere raggiungere il suo obiettivo,
*   Chiaro per il lettore.
*   Congruente con i costi di progetto (o l’ipotesi di budget nel caso che si tratti di budgeting).
*   Con una corretta distribuzione dei carichi fra le diverse figure professionali coinvolte.

Credo che in molti ritengano la pianificazione dell’intero progetto in ogni suo minimo dettaglio antistorica rispetto alle moderne metodologie di conduzione dei progetti ([XP](http://en.wikipedia.org/wiki/Extreme_Programming) o _[agile](http://en.wikipedia.org/wiki/Agile_software_development)_ che dir si voglia).

Ciò detto con una certa frequenza mi capita di mettere mano allo _[strumento del demonio](http://www.microsoft.com/project)_ quanto segue è il sunto delle varie prove ed errori mi è capitato di fare.

**Visualizzazione del [gantt](http://en.wikipedia.org/wiki/Gantt_chart):** aggiungo sempre la colonna del workload per:

*   Potere inserire velocemente lo sforzo necessario per portare a termine il task
*   Verificare facilmente che, dopo avere definito le risorse, la duration cambi conseguentemente
*   Evitare che accidentalmente (modifico le risorse, poi ritocco la duration) venga modificato il workload (se sto facendo budgeting è sicuramente un disastro, se si tratta di conduzione del progetto in essere forse no).

**Opzioni di default:** nelle opzioni di MSProject imposto

[![](http://2.bp.blogspot.com/_RjgCgazXb04/Rb_DWASYoYI/AAAAAAAAAAM/Q8gWMxngcjk/s320/clip_image002.jpg)](http://2.bp.blogspot.com/_RjgCgazXb04/Rb_DWASYoYI/AAAAAAAAAAM/Q8gWMxngcjk/s1600-h/clip_image002.jpg)

Dal tab “schedule”:

1.  “days” come unità di inserimento del workload,
2.  “Assigment units” decimali (non sopporto vedere l’allocazione del 200% di uno sviluppatore Java per esprimere che ne sono necessari due).
3.  Default task type “Fixed Work” è il più adatto secondo me alla valutazione dell’effort per i progetti software.

Nel tab “Calendar” imposto le giornate che iniziano alle 9:00 e finiscono alle 18:00 (faccio sempre in tempo ad aggiungere extra hours per ciascun task o in ciascun giorno del calendario).

**Struttura del piano**: usa una struttura ad albero con una radice: cioè definisci un macro task che includa tutti gli altri. In questo modo avrai sempre sott’occhio data di inizio e fine, effort e durata in giorni complessivi.

[![](http://3.bp.blogspot.com/_RjgCgazXb04/Rb_DlQSYoZI/AAAAAAAAAAU/Y8sFkpdnvM4/s320/clip_image004.gif)](http://3.bp.blogspot.com/_RjgCgazXb04/Rb_DlQSYoZI/AAAAAAAAAAU/Y8sFkpdnvM4/s1600-h/clip_image004.gif)

**Task:** definisci tutte le attività che hanno pertinenza con il livello di dettaglio con cui vuoi illustrare lo svolgersi dei lavori; **non dimenticare** di aggiungere tempo a sufficienza per l’installazione e configurazione degli ambienti di sviluppo, la configurazione degli strumenti di supporto allo sviluppo (bugtracking, source code repository, wiki di progetto o altro strumento di gestione documentale, ...) i test, l’installazione e configurazione degli ambienti di produzione, l’installazione dei servizi applicativi e infine il collaudo.

Stavo quasi per dimenticare che tra le attività c’è sicuramente il **coordinamento** ... _non credo che ci sia modo di definire che ha inizio con l’inizio del progetto e termina con l’ultima attività del progetto medesimo comportando il lavoro del x% di un project manager_ ... ma se scopri come si fa dimmelo.

**Dipendenze tra task**: in genere cerco di fare in modo che le dipendenze siano definite innanzitutto fra task del medesimo livello, in secondo luogo fra task di un livello quelli di livello superiore (“_il task 1.2.3 non può essere completato senza che venga prima portato a termine il task 1.1_”).

Non vedo perché non rappresentare dipendenze all’indietro (“_il task 1.2.3 dipende dal completamento del task 1.3_”) ma credo che, se non definisci le priorità e non configuri di conseguenza le opzioni di leveling, l’operazione di leveling possa dare risultati ancora più aleatori della norma.

**Risorse:** definisci sempre chi deve fare cosa, è più semplice farlo prima che dopo, inoltre è più chiaro **cosa combina project**: l’unica formula che conosce è la democraticissima:

**D = W/R**

la duration è pari al carico di lavoro suddiviso fra le risorse assegnate.

**Vincoli temporali**: usane il meno possibile oppure evita che “_tasks will always honor their constraint date_”: quando devi inserire un vincolo temporale del tipo “deve terminare non oltre il gg/mmm/yyyy” pensa se non è possibile invece utilizzare una milestone: è molto discreta, non è visibile fintanto che viene rispettata, altrimenti è visualizzata chiaramente con un segnale rosso.

[![](http://2.bp.blogspot.com/_RjgCgazXb04/Rb_D-ASYoaI/AAAAAAAAAAc/vpOeLABDlZc/s320/clip_image006.jpg)](http://2.bp.blogspot.com/_RjgCgazXb04/Rb_D-ASYoaI/AAAAAAAAAAc/vpOeLABDlZc/s1600-h/clip_image006.jpg)

**Pianificazione**: definisci un numero alto di risorse disponibili, anche oltre al ragionevole (2 Manager, 30 Sviluppatori, …), lo abbasserai dopo.

Configura il leveling con “look for overallocations on a **Week by Week** basis”: tutte le persone del tuo team sono in grado di gestire il proprio tempo nel corso di una settimana perché tu non gli metta a piano più di 40h la settimana (_si può fare ma in casi estremi, per brevi periodi di tempo e con il consenso di chi deve passare quel tempo a lavorare_ … secondo me è una tecnica da prendere in considerazione solo per i piani da presentare al cliente).

Utilizza “level now”: la sovrapposizione di una coppia di attività in sovrapposizioni per la medesima persona per più di 40h la settimana venga eliminata spostandone una alla settimana successiva. Questa operazione si ripete automaticamente per ciascuna coppia di attività per ciascuna persona.

Quello che ottieni è il piano ideale a tempo infinito in altri termini la risposta alla domanda “quanto tempo per portare a termine il progetto con questo team?”.

I nomi delle persone in sovrallocazione sono colorati di rosso anche se la sovrapposizione è di un solo minuto: quindi non stupirti se nella vista dell’utilizzo delle persone vedi dei nomi in rosso.

Prova a lasciar fare al tool per qualche volta mentre tu modifichi le duration (aumentando il carico sulle risorse; credo che venga cambiato solo il carico della prima risorsa fra quelle assegnate al task!) o la quantità di persone disponibili per ciascuna figura professionale.

Puoi tentare di rispondere alla domanda “quante persone per ciascuna figura professionale devo assoldare per concludere il progetto entro il gg/mmm/yyyy?”.

Dalle proprietà del progetto imposta la data di fine al posto di quella di inizio delle attività e lascia fare a Project.

Sinceramente non mi è mai capitato di usare questo strumento per rispondere alla domanda “quanto lavoro è necessario per portare a termine il progetto dato un team prestabilito e una data di chiusura prefissata?”.

In conclusione non fidarti troppo di quanto da Project da solo (è solo software), dagli una stampata, appendilo al muro e ripensaci il giorno dopo magari una bella sessione a due.

Può essere utile prodursi un template con i propri default preimpostati e perchè no, le figure professionali normalmente utilizzate con i relativi costi (ratecard) e magari una macro struttura di progetto preimpostata.