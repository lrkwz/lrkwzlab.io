---
layout: post
title: Booch poeta
date: '2005-10-06T11:13:00.001+02:00'
author: Luca Orlandi
lang: it-IT
tags:
modified_time: '2006-10-26T01:02:59.276+02:00'
blogger_id: tag:blogger.com,1999:blog-5600070.post-112859001178012194
blogger_orig_url: http://lrkwz.blogspot.com/2005/10/booch-poeta.html
---

Solo oggi riesco a leggere [un post dal blog di Grady Booch](http://4-000000004459AE3CD983D24FB2C746FCB092F90407000C109A1DC452EB4CABEC9E8CCDA6FA0A00000001F9D100002A8CA77D492EE6459D1909EE645F130A00000BDD824E0000/www-106.ibm.com/developerworks/blogs/dw_blog_comments.jspa?blog=317&entry=96353&ca=drs-bl): lo trovo quasi poetico quando dice:

"_It seems to me that there's a curious relationship between dance and software architecture: in both disciplines, there are only a limited number of patterns available, each genre assembles those patterns in specific ways that define a particular genre, and furthermore, most knowledge is passed on through tribal memory, from one dancer or architect to another. In dance, by the way, some attempts have been made to define a graphical notation (labanotation) for describing dance movements, just as we have the UML for visualizing, specifying, constructing, and documenting software-intensive systems._"

I blog di Booch da un lato e Irving Wladawsky-Berger dall'altro mi sembrano sempre più ricchi di spunti interessanti.