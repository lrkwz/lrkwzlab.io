---
layout: post
title: I migliori sw per windows di Andrea Beggi
date: '2006-01-17T14:13:00.000+01:00'
author: Luca Orlandi
lang: it-IT
tags:
modified_time: '2006-10-26T01:02:59.527+02:00'
blogger_id: tag:blogger.com,1999:blog-5600070.post-113750363805687238
blogger_orig_url: http://lrkwz.blogspot.com/2006/01/i-migliori-sw-per-windows-di-andrea.html
---

Ogni anno qualcuno ci prova ad ogni modo ecco una recensione che
condivido per un buon 70% open source o freeware nella peggiore delle
ipotesi.

L'elenco è diviso in tre post diversi: [Parte 1
](http://www.andreabeggi.net/2006/01/04/beggi-softlist-i-migliori-programmi-per-windows-1-di-3/), [Parte 2](http://www.andreabeggi.net/2006/01/05/beggi-softlist-i-migliori-programmi-per-windows-2-di-3/), [Parte 3](http://www.andreabeggi.net/2006/01/05/beggi-softlist-i-migliori-programmi-per-windows-3-di-3/)