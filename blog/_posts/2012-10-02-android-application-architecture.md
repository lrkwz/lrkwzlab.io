---
layout: post
title: Android application architecture
date: '2012-10-02T22:06:00.000+02:00'
author: Luca Orlandi
lang: en-US
tags:
- Tech
- android
modified_time: '2013-01-06T23:07:47.761+01:00'
blogger_id: tag:blogger.com,1999:blog-5600070.post-6035648973281609179
blogger_orig_url: http://lrkwz.blogspot.com/2012/10/android-application-architecture.html
---

My professional background roots in the [late 80's when Windows 1.x](http://en.wikipedia.org/wiki/History_of_Microsoft_Windows#Windows_1.0_and_Windows_2.0) arrived in Italy, some years later  I turned to a server-side development and architecture (C++/C# somehow has been replaced by Java and his friends). But somewere in my hearth still the "client side user experience" rocks.
So looking for something relatively new I have found a very interseting  series about android architecture:

1.  [Background](http://vladnevzorov.com/2011/04/18/android-application-architecture-part-i-background/)
2.  A[rchitectural styles and patterns](http://vladnevzorov.com/2011/04/30/android-application-architecture-part-ii-architectural-styles-and-patterns/)
3.  [Main parts of the applications](http://vladnevzorov.com/2011/05/18/android-application-architecture-part-iii-main-parts-of-the-applications/)
4.  [Application integration layer](http://vladnevzorov.com/2011/07/04/android-application-architecture-part-iv-android-application-integration-layer/)

It is very useful in my opinion  understand why and WHEN some architectural choices have been made: this can drive how you design your application and how you write your own code for that specific operating system.

Thank you Vlad! loooking forward for [part V](http://vladnevzorov.com/tag/android-application-architecture/).

[![Enhanced by Zemanta](http://img.zemanta.com/zemified_h.png?x-id=f2a462c9-5f84-4f8a-9418-ab8d662652f8)](http://www.zemanta.com/?px "Enhanced by Zemanta")