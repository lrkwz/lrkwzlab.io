---
layout: post
title: Lambrate, un passaggio nel tempo e nello spazio
date: '2021-04-02'
author: Luca Orlandi
tags:
- fotografia
- photography
- street-photography
- b&w
---

Queste immagini le ho scattate a Lambrate, nel passaggio della stazione; un "non luogo" come molti altri eppure fortemente caratterizzato e riconoscibile per coloro i quali lo attraversano quotidianamente, stancamente, lo sguardo rivolto a terra non si accorgono di cosa accate nel loro ambiente.

![2](../assets/img/lambrate-2021/DSC06731.jpg)
![3](../assets/img/lambrate-2021/DSC06732.jpg)

Una uscita imprevista.

![4](../assets/img/lambrate-2021/DSC06733.jpg)

Normalmente le persone qui si evitano, la distanza imposta accentua la diffidenza la distanza fra i corpi aumenta e un senso di disagio pervade chi si incrocia nello stretto corridoio.

![5](../assets/img/lambrate-2021/DSC06734.jpg)
![1](../assets/img/lambrate-2021/DSC06737.jpg)
![1](../assets/img/lambrate-2021/DSC06730.jpg)
