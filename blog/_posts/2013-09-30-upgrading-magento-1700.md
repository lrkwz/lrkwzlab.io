---
layout: post
title: Upgrading magento 1.7.0.0
date: '2013-09-30T12:45:00.004+02:00'
author: Luca Orlandi
tags:
- php
- magento
modified_time: '2014-05-13T12:00:32.077+02:00'
blogger_id: tag:blogger.com,1999:blog-5600070.post-6538128574123355155
blogger_orig_url: http://lrkwz.blogspot.com/2013/09/upgrading-magento-1700.html
---

This trivial note just because the [wiki page about upgrading magento](http://www.magentocommerce.com/wiki/1_-_installation_and_configuration/upgrading_magento#magento_upgrade_guide_for_1.3.x_1.4.x_1.5.x_1.6.x_and_1.7.x) seems to be quite outdated ... or let's say it clearly ... wrong.

To upgrade magento from the command line (you would like to do it in case you have no public ftp access)

```
cd $YOUR\_MAGENTO\_HTDOCS chmod 766 mage ./mage mage-setup ./mage sync ./mage list-upgrades ./mage upgrade-all
```

this upgrades all the extensions and the core application.

This utility can also be used to install extensions:

    ./mage install community