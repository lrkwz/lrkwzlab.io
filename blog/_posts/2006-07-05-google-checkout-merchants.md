---
layout: post
title: Google Checkout - Merchants
date: '2006-07-05T23:55:00.000+02:00'
author: Luca Orlandi
tags:
- Tech
modified_time: '2006-10-26T01:03:00.633+02:00'
blogger_id: tag:blogger.com,1999:blog-5600070.post-115213654135814998
blogger_orig_url: http://lrkwz.blogspot.com/2006/07/google-checkout-merchants.html
---

Ho visto la presentazione del nuovo servizio [Google Checkout - Merchants](http://checkout.google.com/sell).

Secondo me stanno esagerando: vendono lo spazio pubblicitario a dei merchant online.
Questi merchant possono usufruire di un supporto alle transazioni (pagamento, gestione ordini, ...) a prezzi agevolati.

Quanto manca alla forte influenza sui risultati delle ricerche del più famoso e potente motore di ricerca?

E' ora di trovare un'alternativa a [Google](http://www.googl.com).