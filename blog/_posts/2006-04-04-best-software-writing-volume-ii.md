---
layout: post
title: Best Software Writing - Volume II
date: '2006-04-04T16:33:00.000+02:00'
author: Luca Orlandi
lang: it-IT
tags:
- Tech
modified_time: '2006-10-26T01:03:00.236+02:00'
blogger_id: tag:blogger.com,1999:blog-5600070.post-114416122639852889
blogger_orig_url: http://lrkwz.blogspot.com/2006/04/best-software-writing-volume-ii.html
---

Ancora una citazione di [Joel](http://www.joelonsoftware.com/items/2006/03/31.html); ha iniziato a raccogliere le candidature per un nuovo libro sulla scrittura del software e nell'annunciarlo riporta una citazione del precedente volume che riporto di seguito:

> The software development world desperately needs better writing. If I have to read another 2000 page book about some class library written by 16 _separate_ people in broken [ESL](http://en.wikipedia.org/wiki/ESL/ESOL/EFL/ELT), Im going to flip out. If I see another hardback book about object oriented models written with dense [faux](http://en.wikipedia.org/wiki/Faux)\-academic pretentiousness, Im not going to shelve it any more in the Fog Creek library: its going right in the recycle bin. If I have to read another spirited attack on Microsofts buggy code by an enthusiastic nine year old [Trekkie](http://en.wikipedia.org/wiki/Trekkie) on Slashdot, I might just poke my eyes out with a sharpened pencil. Stop it, stop it, stop it!

Che dire, sono 100% d'accordo, ritengo che leggere libri (o anche solo pezzi di libri) sul sw sia un forte acceleratore di conoscenza, idee ecc ma c'è in giro una marea di spazzatura o libri semplicemente pretenziosi.