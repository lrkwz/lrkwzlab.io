---
layout: post
title: Tido
date: '2002-09-17T18:24:00.000+02:00'
author: Luca Orlandi
lang: it-IT
tags:
- tido
modified_time: '2006-10-26T01:02:59.666+02:00'
blogger_id: tag:blogger.com,1999:blog-5600070.post-114295099103995703
blogger_orig_url: http://lrkwz.blogspot.com/2002/09/eccomi-qui-dopo-due-mesi-passati-tempo.html
---

Eccomi qui dopo due mesi passati a tempo pieno con te sono tornato in ufficio ... stavo meglio con te.
Queste lettere sono in qualche modo ispirate da un predecessore ben piu' blasonato e al momento le sue lettere sono disponibili su uno dei siti della [Nasa](http://spaceflight.nasa.gov/history/shuttle-mir/history/h-f-linenger-letters.htm).

In questi due mesi hai mparato a fare un sacco di cose e mi hai commosso e stupito un'infinita' di volte: vorrei cercare di raccontarle tutte.