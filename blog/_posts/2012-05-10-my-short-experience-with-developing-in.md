---
layout: post
title: My (short) experience with developing in the cloud(s)
date: '2012-05-10T12:00:00.000+02:00'
author: Luca Orlandi
lang: en-US
tags:
modified_time: '2012-05-10T12:06:00.536+02:00'
blogger_id: tag:blogger.com,1999:blog-5600070.post-7754710799328958347
blogger_orig_url: http://lrkwz.blogspot.com/2012/05/my-short-experience-with-developing-in.html
---

As some of you know I've released an open source [integration for spring with jCryption](http://lrkwz.github.com/jCryptionSpring/).
In the very beginning of this story I've hosted my source code on gitorious then I switched to github because of a better integrated environment (issues, wiki, ...) and generally speaking a much more "_alive environment_".

Today I've been astonished by the features given to free accounts on [cloudbees](https://www.cloudbees.com/) PaaS!

In few minutes I've been able to clone my git repository to cloudbees (still maintaining the github one ... and the gitorios as a sort of thermonuclear disaster recovery plan), create a jenkins build job and deploy my app [into the cloud](http://jcryptionspring-sample.lrkwz.cloudbees.net/).
Next will be the maven artifact repository use, now I'm using the sonatype one to deliver release artifacts to maven central ... see you!