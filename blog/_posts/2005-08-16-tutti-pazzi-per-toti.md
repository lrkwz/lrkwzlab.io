---
layout: post
title: Tutti pazzi per Toti
date: '2005-08-16T23:35:00.000+02:00'
author: Luca Orlandi
lang: it-IT
tags:
modified_time: '2006-10-26T01:02:59.078+02:00'
blogger_id: tag:blogger.com,1999:blog-5600070.post-112422815554574350
blogger_orig_url: http://lrkwz.blogspot.com/2005/08/tutti-pazzi-per-toti.html
---

Sì, lo ho visto anche nella notte dell'11/8/2005 del quando transitava lungo la Paullese diretto in via Toffetti: il "mitico Toti" un cosiddetto "rediduato bellico gloria della marina militare italiana" ... Costruito nel 1967 ... Uno dei commenti più frequenti della bella folla che lo accompagna per ciascun metro del suo tragitto è "... È l'unico sommergibile che non ci hanno affondato" ... Ma chi diavolo ha affondadato un sommergibile italiano dal 1967 ad oggi?

Bella umanità a parte: perché non hanno usato quei soldi (ho capito bene? 3.5 milioni di euro?) per migliorare i musei esistenti e costruirne uno navale nel paese di provenienza del povero natante?

La avete sentita la testimonianza della fidanzatina sedicenne di allora che viene portata in visita al sommergibile dopo la sua inauguraizione? Fantastica! È 'unica cosa bella che mi è capitato di sentire in questi giorni su questo argomento.