---
layout: post
title: Installing magento 1.3.0 on ubuntu 11.10
date: '2012-02-10T15:59:00.001+01:00'
author: Luca Orlandi
lang: en-US
tags:
- ubuntu
- php
- magento
modified_time: '2012-02-10T16:00:16.591+01:00'
blogger_id: tag:blogger.com,1999:blog-5600070.post-3338600626578320562
blogger_orig_url: http://lrkwz.blogspot.com/2012/02/installing-magento-130-on-ubuntu-1110.html
---

I aim to be 100% a software architect, eventually isn't possible: a complete architect should be a polyglot software developer a good designer and also a decent system engineer.

That's why from time to time I challenge the installation of strange beasts, recently I faced [magento ecommerce](http://www.magentocommerce.com/) ... well unfortunately an old version of magento still bound to PHP 5.2. As most of you know php 5.2 is gone; php 5.3 is packaged out of the box with many Linux distributions and has many incompatibilities with php 5.2 (actually patches for magent exists but my first choice was installing the system on his barebone prerequisites).

Googling you can find instructions to install php 5.2 from binary packages in many distributions including ubuntu 10.x but all fail in some way when you have to catch extension modules as mcrypt or gd. At the end I made my mind to compile php from scratch, following are my notes in the hope that somebody out there will spend less time to achieve the same result.

My target machine is:

```
$ uname -a
Linux mymachine-dev 3.0.0-15-server #26-Ubuntu SMP Fri Jan 20 19:07:39 UTC 2012 x86\_64 x86\_64 x86\_64 GNU/Linux

$ lsb\_release -a
No LSB modules are available.
Distributor ID:    Ubuntu
Description:    Ubuntu 11.10
Release:    11.10
Codename:    oneiric
```

I had to install a few packages in order to complete my job:

```
sudo apt-get install gcc
sudo apt-get install make
sudo apt-get install mysql-server
sudo apt-get install apache2
sudo apt-get install apache2-prefork-dev
sudo apt-get install apache2-mpm-prefork
sudo apt-get install mysql-server
sudo apt-get install libxml2-dev
sudo apt-get install libcurl4-openssl-dev
sudo apt-get install libpng3 libpng12-dev
sudo apt-get install libmcrypt-dev
sudo apt-get install libltdl-dev
```

the trikiest part has been the graph library which needed an hand made extra symbolic to have php's configure tool find it:

```
sudo apt-get install libjpeg8-dev
sudo ln -s /usr/lib/x86\_64/libpng.so /usr/lib/
```

Now you can ddownload and un-tar php itself:

```
wget http://it2.php.net/get/php-5.2.17.tar.bz2/from/this/mirror
tar -xjvf php-5.2.17.tar.bz2
cd php-5.2.17
```

finally configure and build!

```
./configure --with-apxs2=/usr/bin/apxs2 --with-config-file-path=/etc/php5/apache2 --with-mysql --with-curl --with-mcrypt --with-pdo-mysql --with-gd
make
sudo make install
```

configure and start the http server

```
sudo service apache2 stop

# enable php module
cd /etc/apache2/mods-enables
sudo ln -s ../mod-available/php.load .

sudo service apache2 start
```

Verify the results using the [compatibility check](http://www.magentocommerce.com/knowledge-base/entry/how-do-i-know-if-my-server-is-compatible-with-magento) kindly provided by magento

```
cd /var/www
wget http://www.magentocommerce.com/\_media/magento-check.zip
sudo apt-get install unzip
sudo unzip magento-check.zip
```

Now point your browser to http://localhost:8080/magento-check.php and check the results, if you are lucky enough you have a chance that all requirements are met!