---
layout: post
title: Un anno
date: '2002-11-04T16:44:00.000+01:00'
author: Luca Orlandi
lang: it-IT
tags:
- tido
modified_time: '2006-10-26T01:02:59.736+02:00'
blogger_id: tag:blogger.com,1999:blog-5600070.post-114295114398926317
blogger_orig_url: http://lrkwz.blogspot.com/2002/11/oramai-hai-pi-di-un-anno-e.html
---

Oramai hai più di un anno e progressivamente hai preso ad occupare uno spazio sempre più importante nella nostra vita; sembrava incredibile averti con noi già quando eri un esserino a malapena sveglio qualche ora al giorno e invece questa sensazione di completezza ha continuato a crescere con te.
Hai uno strano carattere: a momenti sembri pensierosa in altri giochi serena oppure fai dei capricci testardi salvo poi distrarti completamente e ancora con i lacrimoni entusiasmarti per un nonnnulla.

Vorrei raccontarti di mio padre, tuo nonno Raffaele o nonno Fefè come te lo sto presentando; cercherò di farlo pian piano, vorrei ricordare tutto quello che è stato per me per poterteno narrare ad ogni modo sono certo che sto facendo con te un poco di quello che lui è stato per me quando ero piccino e lui "papà".