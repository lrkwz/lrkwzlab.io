---
layout: post
title: Microservizi e sistemi ad eventi
date: '2021-01-30'
author: Luca Orlandi
lang: it-IT
tags:
- microservice
- spring
- springboot
- java
---

Molto bello ["Microservizi in Java con Spring Boot e Spring Cloud"](https://codingjam.it/microservizi-in-java-con-spring-boot-e-spring-cloud/), una descrizione chiara semplice ed esaustiva delle architetture a microservizi. Quando poi ti vengono presentati i personaggi della storia:

* Eureka (service registry)
* Hystrix (circuit breaker)
* Ribbon (nastro) client side load balancer
* Feign (fingere/simulare) client REST
* Zulu API gateway
* Sleuth (investigatore) log aggregator

il tutto assume l'aspetto di un romanzo.