---
layout: post
title: La scelte di design hanno un costo
date: '2011-12-03T21:11:00.001+01:00'
author: Luca Orlandi
tags:
- design
- maven
modified_time: '2011-12-04T17:44:02.313+01:00'
thumbnail: http://1.bp.blogspot.com/-mE94_sa9SOU/TtqPIJR0tkI/AAAAAAAAB2Y/zxY0XBbO3LA/s72-c/IMG_1357_web.JPG
blogger_id: tag:blogger.com,1999:blog-5600070.post-1435332647122298459
blogger_orig_url: http://lrkwz.blogspot.com/2011/12/la-scelte-di-design-hanno-un-costo.html
---

[![](http://1.bp.blogspot.com/-mE94_sa9SOU/TtqPIJR0tkI/AAAAAAAAB2Y/zxY0XBbO3LA/s1600/IMG_1357_web.JPG)](http://1.bp.blogspot.com/-mE94_sa9SOU/TtqPIJR0tkI/AAAAAAAAB2Y/zxY0XBbO3LA/s1600/IMG_1357_web.JPG)

Il design del software è indubbiamente un'arte (

> _L'**arte**, nel suo significato più ampio, comprende ogni attività umana - svolta singolarmente o collettivamente - che porta a forme [creative](http://it.wikipedia.org/wiki/Creativit%C3%A0 "Creatività") di espressione [estetica](http://it.wikipedia.org/wiki/Estetica "Estetica"), poggiando su accorgimenti tecnici, abilità innate e norme comportamentali derivanti dallo [studio](http://it.wikipedia.org/wiki/Studio_(apprendimento) "Studio (apprendimento)") e dall'[esperienza](http://it.wikipedia.org/wiki/Esperienza "Esperienza")._

fonte wikipedia ovviamente), le scelte che fai però non sono gratis, accade che se ne debbano pagare le conseguenze  (probabilmente accede anche nell'arte in senso stretto ma questo non è affar mio). Per fare un esempio mi è capitato di dovere progettare e realizzare un sistema che prevedeva due interfaccie differenti che insistevano sul medesimo gruppo di domain objects: da una parte un normale sito web, dall'altra delle api raggiungibili tramite AMF (un protocollo binario incapsulato all'interno dell'HTTP) a supporto di una applicazione AIR.
Fin dall'inizio è stato chiaro che le necessità di intervento su una e sull'altra avrebbero subito i tempi e i modi di due diverse classi di utenti, insomma le due applicazioni avrebbero vissuto una vita propria ed indipendente l'una dall'altra.
Restava il fatto che avrebbero entrambe insistito sui medesimi oggetti, pertanto ho deciso di implementare un modulo "core", pacchettizzato sotto forma di libreria,  per gli oggetti di dominio e le logiche di business comuni e un modulo per ciascuna delle due applicazioni; queste avrebbero così potuto essere modificate e rilasciate (il processo di deploy di queste applicazioni è abbastanza rigido lungo e laborioso per ragioni che non mi dilungo ad illustrare) l'una indipendentemente dall'altra.
Fantastico, il design e la modularizzazione mi sono sembrati impeccabili. Peccato che dopo poco mi sono reso conto che il processo di modifica al modulo core, per esempio a causa di un nuovo requisito di una delle due applicazioni del quale avrebbe potuto beneficiare anche l'altra o a causa di un bug fix, comportava appunto la modifica e compilazione della libreria, la sua installazione sul repository locale e la successiva modifica della applicazione sulla quale si stava intervenendo. Tutto ciò nonostante tutti i moduli fossero presenti all'interno dell'IDE (eclipse anzi la sua mutazione che prende il nome di STS).
Insomma per farla breve questa modularizzazione comporta un piccolo ma costante dispendio di tempo per ciascuna modifica del modulo comune; tutti questi piccoli dispendi di tempo sommati nei mesi diventano ovviamente ... un grosso dispendio di tempo!


[![](http://3.bp.blogspot.com/-cvFvCiCZf60/TtqWbKK0GjI/AAAAAAAAB2g/PGo26PXFpDM/s320/IMG_1396_web.JPG)](http://3.bp.blogspot.com/-cvFvCiCZf60/TtqWbKK0GjI/AAAAAAAAB2g/PGo26PXFpDM/s1600/IMG_1396_web.JPG)

Recentemente ho avuto modo di mettere mano su [jrebel](http://zeroturnaround.com/jrebel/) per un progettino open source che ho deciso di rilasciare. Ammetto di essere stato mosso da semplice curiosità da nerd; inizialmente ho fatto una discreta fatica per apprezzarne i benefici ma nonappena ho iniziato a modularizzare tutto è diventato chiaro: jrebel mi consentiva di saltare uno dei passaggi della catena!
Una volta [configurato](http://zeroturnaround.com/reference-manual/app.html#app-3.7), jrebel si fa carico della responsabilità di monitorare i cambiamenti all'interno dei moduli di base comuni a tutte le applicazioni evitando di dovere installare nel repository locale ogni qualvolta di fa una piccola modifica.
Per esempio puoi eseguire l'applicazione con mvn tomcat:run e modificare i moduli esterni senza dovere fermare e riavviare l'app server ad ogni ricompilazione delle librerie. jrebel si occupa di monitorare tutte le risorse necessarie ad eseguire l'applicazione e ricaricare quel che serve.
Il sistema non funziona sempre, ogni tanto dovrai comunque fermare e riavviare l'app server ma il numero di reload scende drasticamente.

Resta inteso che di norma considero obbligatorio utilizzare maven e quindi sottostare ad un rigido processo "compile-test-install-release" per tutti i rilasci verso l'esterno in modo che tutti gli sviluppatori del team possano avere meggiore confidenza sulla bontà del software realizzato dagli altri.

Per i progetti commerciali jrebel ha un costo (a mio avviso assolutamente proporzionale al tempo risparmiato), per i progetti open source è gratuito e recentamente hanno introdotto una "licenza social" che sostanzialmente consente di utilizzare il prodotto a fronte della sponsorizzazione (automatica)  sui social network che utilizzi comunemente.

PS le foto sono scattate rispettivamente alla [casa dell'energia](http://www.casadellenergia.it/) e alla [mostra sull'alimentazione](http://www.museoscienza.org/attivita/mostre/buon-appetito/) al museo della scienza e della tecnica