---
layout: post
title: GooSync sincronizza Google Calendar con il telefono cellulare o il PDA
date: '2006-11-26T00:31:00.001+01:00'
author: Luca Orlandi
tags:
modified_time: '2006-11-27T17:16:57.002+01:00'
blogger_id: tag:blogger.com,1999:blog-5600070.post-8709271180054042023
blogger_orig_url: http://lrkwz.blogspot.com/2006/11/goosync-sincrionizza-google-calendar.html
---

GooSync sincronizza Google Calendar con il telefono cellulare o il PDA il funzionamento è abbastanza elementare:

*   conosce username e password per accedere al servizio del calendario
*   ne legge i dati
*   A seconda del device possono accadere cose diverse

> _**\- Your device supports over-the-air configuration:**
> We will send you a configuration message by sms, just open and save the received message to auto configure your device._
>
> _**\- Your device requires manual configuration:**__
> We will display generic manual instructions, follow these to manually configure your device._
>
> _**\- Your device is a Palm or Windows device:**
> We will display the download and install instructions, follow these to manually setup your device_

Basta fare la registrazione per accorgersene ... peccato che per esempio l'update manuale sembra non avere alcuna intenzione di funzionare.

[![](http://photos1.blogger.com/x/blogger2/4934/683/320/509932/ScreenShot003.jpg)](http://photos1.blogger.com/x/blogger2/4934/683/1600/812299/ScreenShot003.jpg)