---
layout: post
title: 'Tiscali ADSL: problemi di configurazione DNS'
date: '2006-07-11T23:33:00.000+02:00'
author: Luca Orlandi
tags:
- Tech
modified_time: '2006-10-26T01:03:00.775+02:00'
blogger_id: tag:blogger.com,1999:blog-5600070.post-115265360927795545
blogger_orig_url: http://lrkwz.blogspot.com/2006/07/tiscali-adsl-problemi-di.html
---

Da qualche settimana ho acquistato un'offerta telefonia + ADSL da Tiscali. Utilizzo con soddisfazione il router VoIP (un Pirelli NetGate VoIP v.2) e navigo a una velocità soddisfacente.
Per utilizzare il tutto senza intoppi ho modificato i collegamenti della mia centrale di allarme e collegato il combinatore telefonico alla linea VoIP \[funziona bene sia per gli allarmi che per i comandi remoti della centrale\] e alimentato il router con la batteria tampone della medesima centrale.

Da qualche giorno il primo problema: il client del firewall Cisco che utilizzo per lavorare (aimè) da casa fa le bizze: posso accedere ai server via ssh utilizzando l'indirizzo IP e non il nome dell'host.
Provo a _pingare_ le macchine della rete privata in ufficio e rispondono tutti con il medesimo indirizzo IP!

Credo di avere finalmente capito quale è la situazione:

1.  il DNS dietro al mio router risolve qualunque nome di host sconosciuto (per esempio una macchina appartenente ad una rete privata) con il medesimo indirizzo ip [62.210.183.10](http://62.210.183.10)
2.  l'indirizzp ip [62.210.183.10](http://62.210.183.10) corrisponde all'host [navigationhelp.tiscali.it](http://navigationhelp.tiscali.it)
3.  il servizio HTTP con il vs motore di ricerca è installato sull host [navigationhelp.tiscali.it](http://navigationhelp.tiscali.it)


Tutto questo meccanismo risolve  elegantemente il problema "business" di portare gli utenti su un sito tiscali invece che mostrare la pagine di errore del browser ma ... il problema è risolto a livello di protocollo IP e non a livello HTTP !

Ritengo che questo problema debba essere risolto differentemente e non a scapito della qualità del servizio che offrite; tra le prime alternative che mi vengono  in mente:

*   setup che modifica le configurazioni del browser in modo da mostrare una "pagina tiscali"
*   installazione di un browser customizzato
*   gestione di un proxy HTTP fra la sottorete tiscali e la Rete esterna
*   ...


PS: Per quanto ne capisco io la soluzione al mio problema la posso ottenere solo rinunciando al servizio DHCP del router che mi avete fornito e cablando nella 'alternate IP configuration' del mio notebook l'indirizzo IP di un DNS configurato correttamente.