---
layout: post
title: World music italiana
date: '2006-11-22T00:14:00.000+01:00'
author: Luca Orlandi
tags:
modified_time: '2006-11-22T02:13:55.376+01:00'
blogger_id: tag:blogger.com,1999:blog-5600070.post-3252290541974853763
blogger_orig_url: http://lrkwz.blogspot.com/2006/11/world-music-italiana.html
---

Imperdibile sul [sito di national geographic!](http://worldmusic.nationalgeographic.com/worldmusic/view/page.basic/country/content.country/italy_346) Ebbene si lo ammetto sono un ascoltatore storico della [sacca del diavolo](http://www.radiopopolare.it/trasmissioni/la-sacca-del-diavolo/).

E per rendere onore ad un collega qualcosina dalla [mongolia](http://worldmusic.nationalgeographic.com/worldmusic/view/page.basic/country/content.country/mongolia_541).