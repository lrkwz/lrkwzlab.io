---
layout: post
title: bin-with-dependencies
date: '2011-06-28T00:57:00.002+02:00'
author: Luca Orlandi
tags:
- Tech
- maven
- java
modified_time: '2011-12-03T22:06:50.700+01:00'
blogger_id: tag:blogger.com,1999:blog-5600070.post-7015702880320078461
blogger_orig_url: http://lrkwz.blogspot.com/2011/06/bin-with-dependencies.html
---

Mi sono trovato a dovere costruire una applicazione java da eseguire da command line appoggiata su un modulo java spring-based.
Dopo avere fatto diversi tentativi ho  scelto di utilizzare [sazzer](https://github.com/sazzer/spring-cli).
Tra i compiti di questo applicativo c'è la codifica di alcune informazioni operazione per la quale ho scelto di utsare jasypt e la lireria bouncycastle.


[![](http://t2.gstatic.com/images?q=tbn:ANd9GcQkVQrXXXlJTJKwmGCUIxgqHpPe0kHK0b_Zg1Cmk3bI29GoXrJ8)](http://t2.gstatic.com/images?q=tbn:ANd9GcQkVQrXXXlJTJKwmGCUIxgqHpPe0kHK0b_Zg1Cmk3bI29GoXrJ8)

Quando mi sono trovato a **pacchettizzare** il tutto ovviamente o pensato di sfruttare il maven-assembly-plugin con il descrittore di default [jar-with-dependencies](http://maven.apache.org/plugins/maven-assembly-plugin/descriptor-refs.html#jar-with-dependencies) (qesto come è noto produce un jar contenenti tutte le classi delle dipendenze indicate nel pom.xml).
Il primo problema che ho dovuto affrontare è legato ad un bug del maven-assembly-plugin che compromette i file META-ING/spring.handlers e META-INF/spring.schemas impedendo l'esecuzione del jar prodotto (ammetto di avere prodotto i miei _a mano_ senza usufruire del [maven-shade-plugin](http://maven.apache.org/plugins/maven-shade-plugin/examples/resource-transformers.html)).

Purtroppo il jar così prodotto **non è eseguibile** in quanto la firma contenuta all'interno della libreria bouncycastle non è valida per il mio artifact!

Infine mi sono risolto per copiare tutte le dipendenze assieme al mio jar ed allegare uno script per eseguire la mia applicazione mettendo nel classpath i jar delle dipendenze.

Per completare il tutto ho configurato il maven-assembly-plugin in modo da produrre un file zip (bz2 o gz) contentente il mio jar, le liberie e lo script

```xml
<assembly xmlns="http://maven.apache.org/plugins/maven-assembly-plugin/assembly/1.1.0"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xsi:schemaLocation="http://maven.apache.org/plugins/maven-assembly-plugin/assembly/1.1.0 http://maven.apache.org/xsd/assembly-1.1.0.xsd">
    <id>bin</id>
    <formats>
        <format>tar.gz</format>
        <format>tar.bz2</format>
        <format>zip</format>
    </formats>
    <fileSets>
        <fileSet>
            <directory>${project.basedir}</directory>
            <outputDirectory>/</outputDirectory>
            <includes>
                <include>README\*</include>
                <include>LICENSE\*</include>
                <include>NOTICE\*</include>
            </includes>
        </fileSet>
        <fileSet>
            <directory>${project.build.directory}/classes/META-INF</directory>
            <outputDirectory>/</outputDirectory>
            <includes>
                <include>\*.sh</include>
                <include>\*.bat</include>
            </includes>
        </fileSet>
        <fileSet>
            <directory>${project.build.directory}</directory>
            <outputDirectory>/</outputDirectory>
            <includes>
                <include>\*\*/\*.jar</include>
            </includes>
        </fileSet>
        <fileSet>
            <directory>${project.build.directory}/site</directory>
            <outputDirectory>docs</outputDirectory>
        </fileSet>
    </fileSets>
</assembly>
```

