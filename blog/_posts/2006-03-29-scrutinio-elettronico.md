---
layout: post
title: Scrutinio elettronico?
date: '2006-03-29T00:01:00.000+02:00'
author: Luca Orlandi
lang: it-IT
tags:
modified_time: '2006-10-26T01:03:00.168+02:00'
blogger_id: tag:blogger.com,1999:blog-5600070.post-114358328435399766
blogger_orig_url: http://lrkwz.blogspot.com/2006/03/scrutinio-elettronico.html
---

Ho letto l'inchiesta di [diario](http://www.diario.it) e il post di [Beppe Grillo](http://www.beppegrillo.it/2006/03/ce_uno_strano_o.html) sul tema dello **scrutinio elettronico** per le elezioni del 9/4:
Dal punto di vista _informatico_, non mi sbilancio ancora (le informazioni pubblicate dal settimanale sono troppo scarne e semplificate), ma principale anello debole della catena mi sembra essere il trasporto a mano (di un privato) e privo di una sicurezza fisica delle chiavette USB dal pc del seggio a quello che si occupa dell'invio dal ministero.

Per quanto riguarda la modalità con cui viene attivata l'iniziativa e il suo contesto:

1.  L'appalto è assegnato direttamente e senza una gara (sarebbe obbligatoria! quante ne avete dovute fare per cercare di aggiudicarvi un qualunque pulciosissimo progetto da 500K€? qui hanno assegnato 34M€.
2.  Capo cordata Telecom (ancora?), tra gli altri Accenture e EDS.
3.  Il figlio del ministro Pisanu sarebbe un partner di Accenture.
4.  No comment su Telecom visti i trascorsi.
5.  Il descreto elettorale ha abolito il sorteggio del personale che soprassiede alle operazioni di seggio (scrutatori, ...) a favore dalla nomina diretta.
6.  Dopo lo scrutinio un privato passeggia per il seggio con una chiavetta USB in tasca.

Quindi i politici al governo incaricano clienteralmente delle aziende private mentre i politici eletti nei comuni comuni incaricano chi deve controllarne l'operato a difesa della democrazia.

Stiamo parlando di 11M di voti in tre regioni storicamente considerate _instabili; mica come l'Emilia Romagia!_

A già, tra parentesi, il voto dovrebbe essere la suprema espressione della democrazia :-).

Approfondimenti:
[Apogeonline](http://www.apogeonline.com/webzine/2006/03/28/19/2006032819185)

Technorati: [politica](http://technorati.com/tag/politica).