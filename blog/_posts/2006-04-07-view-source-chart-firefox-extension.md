---
layout: post
title: View Source Chart (Firefox Extension)
date: '2006-04-07T12:30:00.000+02:00'
author: Luca Orlandi
lang: it-IT
tags:
- Tech
modified_time: '2006-10-26T01:03:00.497+02:00'
blogger_id: tag:blogger.com,1999:blog-5600070.post-114440583382636741
blogger_orig_url: http://lrkwz.blogspot.com/2006/04/view-source-chart-firefox-extension.html
---


[View Source Chart](http://jennifermadden.com/scripts/ViewRenderedSource.html) è l'ennesima estensione per firefox ; permette di visualizzare la struttura dei sorgenti delle pagine in modo decisamente chiaro e innovativo


![](http://jennifermadden.com/scripts/vrs.png)