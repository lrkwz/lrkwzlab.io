---
layout: post
title: Amazon Web Services
date: '2011-03-04T13:44:00.001+01:00'
author: Luca Orlandi
tags:
modified_time: '2011-03-04T13:44:47.747+01:00'
thumbnail: https://lh5.googleusercontent.com/--CXmu0HF2vc/TXDbD1tdpqI/AAAAAAAABk8/2eqekKYzVLg/s72-c/Screenshot.png
blogger_id: tag:blogger.com,1999:blog-5600070.post-6516047955051138256
blogger_orig_url: http://lrkwz.blogspot.com/2011/03/amazon-web-services.html
---

Sto provando ad utilizzzare gli amazon web services per alcune applicazioni che sto sviluppando in proprio, trovo il servizio molto interessante per diverse ragioni:

*   amazon è molto rapida ad attivare il servizio, dal momento della richiesta nel giro di qualche ora puoi loggarti via ssh alla tua prima macchina
*   il servizio esiste in diverse regioni del mondo per cui puoi deployare l'applicazione "vicino" al tuo target
*   puoi scalare le dimensioni della macchina molto semplicemente
*   il periodo gratuito copre un interno anno e comunque [i costi](http://www.google.com/search?q=18.25+usd+in+euro&hl=en&num=10&lr=&ft=i&cr=&safe=off&tbs=) per una micro sono paragonabili con i provider di VPS italiani (p.e. [seflow](http://www.seflow.it/vps/))
*   a differenza del [servizio offerto da google](http://appengine.google.com/) non ci sono limitazioni in termini di sviluppo applicativo



Solo una nota, fate attenzione perchè il periodo free è disponibile solo per le macchine più piccole (le _micro_ non le _small_); quando definisci una nuova istanza di quelle con la stelletta "eligible for free period"

[![](https://lh5.googleusercontent.com/--CXmu0HF2vc/TXDbD1tdpqI/AAAAAAAABk8/2eqekKYzVLg/s320/Screenshot.png)](https://lh5.googleusercontent.com/--CXmu0HF2vc/TXDbD1tdpqI/AAAAAAAABk8/2eqekKYzVLg/s1600/Screenshot.png)


il wizard ti propone una small (quindi a pagamento),

 [![](https://lh3.googleusercontent.com/-eetBZd1inJI/TXDbNg_Q-oI/AAAAAAAABlA/9M0r80vELwM/s320/Screenshot-1.png)](https://lh3.googleusercontent.com/-eetBZd1inJI/TXDbNg_Q-oI/AAAAAAAABlA/9M0r80vELwM/s1600/Screenshot-1.png)



ricorda di aprire la tendina e scegliere una **micro**

[![](https://lh6.googleusercontent.com/-4qj-ITXIB6k/TXDbWusnu6I/AAAAAAAABlE/F4ZmNFGHNac/s400/Screenshot-2.png)](https://lh6.googleusercontent.com/-4qj-ITXIB6k/TXDbWusnu6I/AAAAAAAABlE/F4ZmNFGHNac/s1600/Screenshot-2.png)