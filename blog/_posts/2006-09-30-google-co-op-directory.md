---
layout: post
title: Google Co-op - Directory
date: '2006-09-30T23:35:00.000+02:00'
author: Luca Orlandi
tags:
- Tech
modified_time: '2006-10-26T01:03:01.151+02:00'
blogger_id: tag:blogger.com,1999:blog-5600070.post-115965212909487749
blogger_orig_url: http://lrkwz.blogspot.com/2006/09/google-co-op-directory.html
---

L'annuncio della nascita di [Google Co-op](http://www.google.com/coop/directory) deve essermi passato sotto al naso.

I servizi integrati sono molti e diversi sono èiuttosto interessanti.
Sono impressionato dagli sforzi di Google nel raccogliere informazioni sui profili personali dei suoi utenti: stanno sperimentando una varietà di strumenti di profilazione colossale.